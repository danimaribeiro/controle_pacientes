﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class PacienteRepositorio : RepositorioGenerico<Dominio.Paciente>, Dominio.Repositorio.IRepositorioPaciente
    {
        public PacienteRepositorio(NHibernate.ISession sessao) : base(sessao) { }
    }
}
