﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class RepositorioRegistrosAtualizar : RepositorioGenerico<Dominio.Sincronizacao.RegistrosAtualizar>, Dominio.Repositorio.IRepositorioRegistrosAtualizar
    {
        public RepositorioRegistrosAtualizar(NHibernate.ISession sessao) : base(sessao) { }

        public override string Adicionar(Dominio.Sincronizacao.RegistrosAtualizar entidade)
        {
            string id = (string)this.SessionHibernate.Save(entidade);
            return id;
        }
    }
}
