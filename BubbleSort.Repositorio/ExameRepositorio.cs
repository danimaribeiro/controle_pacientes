﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class ExameRepositorio : RepositorioGenerico<Dominio.Exames>, Dominio.Repositorio.IRepositorioExame
    {
        public ExameRepositorio(NHibernate.ISession sessao) : base(sessao) { }
    }
}
