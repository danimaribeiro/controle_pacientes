﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class PacienteMappings : FluentNHibernate.Mapping.ClassMap<Dominio.Paciente>
    {
        public PacienteMappings()
        {
            Table("TB_Paciente");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Cpf).Length(20);
            Map(x => x.Email).Length(50);
            Map(x => x.Nome).Length(100);
            Map(x => x.Observacao).Length(255);
            Map(x => x.Rg).Length(20);
            Map(x => x.TelefoneCelular).Length(15);
            Map(x => x.TelefoneFixo).Length(15);
            References(x => x.Endereco).Column("Endereco_id").Cascade.All();
            References(x => x.Clinica).Column("Clinica_id").Cascade.All();
            References(x => x.Convenio).Column("Convenio_id").Cascade.All();
            References(x => x.Profissional).Column("Profissional_id").Cascade.All();
        }
    }

    public class PacienteServerMappings : FluentNHibernate.Mapping.ClassMap<Dominio.Paciente>
    {
        public PacienteServerMappings()
        {
            Table("TB_Paciente");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Cpf).Length(20);
            Map(x => x.Email).Length(50);
            Map(x => x.Nome).Length(100);
            Map(x => x.Observacao).Length(255);
            Map(x => x.Rg).Length(20);
            Map(x => x.TelefoneCelular).Length(15);
            Map(x => x.TelefoneFixo).Length(15);
            References(x => x.Endereco).Column("Endereco_id").Cascade.All();
            References(x => x.Clinica).Column("Clinica_id").Cascade.All();
            References(x => x.Convenio).Column("Convenio_id").Cascade.All();
            References(x => x.Profissional).Column("Profissional_id").Cascade.All();
        }
    }
}
