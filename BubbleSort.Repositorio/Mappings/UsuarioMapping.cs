﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class UsuarioMapping: FluentNHibernate.Mapping.ClassMap<Dominio.Usuario>
    {
        public UsuarioMapping()
        {
            Table("tb_usuario");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Nome).Length(50);
            Map(x => x.Senha).Length(30);
            Map(x => x.Username).Length(30);
        }
    }

    public class UsuarioServerMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Usuario>
    {
        public UsuarioServerMapping()
        {
            Table("tb_usuario");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Nome).Length(50);
            Map(x => x.Senha).Length(30);
            Map(x => x.Username).Length(30);
        }
    }
}
