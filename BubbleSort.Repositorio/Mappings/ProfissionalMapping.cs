﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class ProfissionalMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Profissionais>
    {
        public ProfissionalMapping()
        {
            Table("TB_Profissional");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Cpf).Length(20);
            Map(x => x.DataNascimento);
            Map(x => x.Email).Length(50);
            Map(x => x.Nome).Length(100);
            Map(x => x.Observacao).Length(255);
            Map(x => x.Rg).Length(15);
            Map(x => x.TelefoneCelular).Length(15);
            Map(x => x.TelefoneFixo).Length(15);
        }
    }

    public class ProfissionalServerMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Profissionais>
    {
        public ProfissionalServerMapping()
        {
            Table("TB_Profissional");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Cpf).Length(20);
            Map(x => x.DataNascimento);
            Map(x => x.Email).Length(50);
            Map(x => x.Nome).Length(100);
            Map(x => x.Observacao).Length(255);
            Map(x => x.Rg).Length(15);
            Map(x => x.TelefoneCelular).Length(15);
            Map(x => x.TelefoneFixo).Length(15);
        }
    }
}
