﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class ConsultasMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Consultas>
    {
        public ConsultasMapping()
        {
            Table("TB_Consultas");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Data);
            Map(x => x.Observacoes).CustomSqlType("ntext");
            Map(x => x.status);
            References(x => x.Clinica).Column("clinica_id");
            References(x => x.Paciente).Column("paciente_id");
            References(x => x.Profissional).Column("profissional_id");
            HasMany(x => x.ListaExames).Cascade.AllDeleteOrphan();
        }
    }

    public class ConsultasServerMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Consultas>
    {
        public ConsultasServerMapping()
        {
            Table("TB_Consultas");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Data);
            Map(x => x.Observacoes).CustomSqlType("ntext");
            Map(x => x.status);
            References(x => x.Clinica).Column("clinica_id");
            References(x => x.Paciente).Column("paciente_id");
            References(x => x.Profissional).Column("profissional_id");
            HasMany(x => x.ListaExames).Cascade.AllDeleteOrphan();
        }
    }
}
