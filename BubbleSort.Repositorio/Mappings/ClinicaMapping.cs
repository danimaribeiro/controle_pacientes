﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class ClinicaMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Clinicas>
    {
        public ClinicaMapping()
        {
            Table("TB_Clinica");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Cnpj).Length(20);
            Map(x => x.InscricaoEstadual).Length(15);
            Map(x => x.Nome).Length(100);
            Map(x => x.NomeFantasia).Length(100);
            Map(x => x.TelefoneClinica).Length(15);
            Map(x => x.TelefoneFax).Length(15);
            References(x => x.Endereco).Column("Endereco_id").Cascade.All();
        }
    }

    public class ClinicaServerMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Clinicas>
    {
        public ClinicaServerMapping()
        {            
            Table("TB_Clinica");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Cnpj).Length(20);
            Map(x => x.InscricaoEstadual).Length(15);
            Map(x => x.Nome).Length(100);
            Map(x => x.NomeFantasia).Length(100);
            Map(x => x.TelefoneClinica).Length(15);
            Map(x => x.TelefoneFax).Length(15);
            References(x => x.Endereco).Column("Endereco_id").Cascade.All();
        }
    }
}
