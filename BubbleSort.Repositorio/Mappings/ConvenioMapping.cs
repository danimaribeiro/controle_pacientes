﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class ConvenioMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Convenios>
    {
        public ConvenioMapping()
        {
            Table("TB_Convenio");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.nomeConvenio).Length(50);
            Map(x => x.observacao).Length(255);
            References(x => x.clinica).Column("clinica_id").Cascade.All();
        }
    }

    public class ConvenioServerMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Convenios>
    {
        public ConvenioServerMapping()
        {
            Table("TB_Convenio");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.nomeConvenio).Length(50);
            Map(x => x.observacao).Length(255);
            References(x => x.clinica).Column("clinica_id").Cascade.All();
        }
    }
}
