﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class RegistrosAtualizarMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Sincronizacao.RegistrosAtualizar>
    {
        public RegistrosAtualizarMapping()
        {
            Table("TB_Registros_Atualizar");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Tabela).Length(30);
            Map(x => x.DataAtualizacao);
            Map(x => x.TipoAtualizacao);
            Map(x => x.uuid).Length(30);
        }
    }

   
}
