﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class EnderecoMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Endereco>
    {
        public EnderecoMapping()
        {
            Table("TB_Endereco");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Bairro).Length(50);
            Map(x=>x.Cep).Length(10);
            Map(x => x.Cidade).Length(50);
            Map(x => x.Estado).Length(30);
            Map(x => x.Numero).Length(50);
            Map(x => x.Rua).Length(100);
        }
    }

    public class EnderecoServerMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Endereco>
    {
        public EnderecoServerMapping()
        {
            Table("TB_Endereco");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Bairro).Length(50);
            Map(x => x.Cep).Length(10);
            Map(x => x.Cidade).Length(50);
            Map(x => x.Estado).Length(30);
            Map(x => x.Numero).Length(50);
            Map(x => x.Rua).Length(100);
        }
    }
}
