﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class UltimaAtualizacaoMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Sincronizacao.UltimaAtualizacao>
    {
        public UltimaAtualizacaoMapping()
        {
            Table("TB_Ultima_Atualizacao");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.DataAtualizacao);            
        }
    }
}
