﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class ExameConsultaMapping : FluentNHibernate.Mapping.ClassMap<Dominio.ExameConsulta>
    {
        public ExameConsultaMapping()
        {
            Table("TB_Exames_Consulta");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Observacao).Length(255);
            References(x => x.Consulta);
            References(x => x.Exame);
        }
    }

    public class ExameConsultaServerMapping : FluentNHibernate.Mapping.ClassMap<Dominio.ExameConsulta>
    {
        public ExameConsultaServerMapping()
        {
            Table("TB_Exames_Consulta");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.Observacao).Length(255);
            References(x => x.Consulta);
            References(x => x.Exame);
        }
    }
}
