﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio.Mappings
{
    public class ExameMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Exames>
    {
        public ExameMapping()
        {
            Table("TB_Exame");
            Id(x => x.Id).GeneratedBy.UuidHex("B");
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.nomeExame).Length(50);
            Map(x => x.observacao).Length(255);
            References(x => x.clinica).Column("clinica_id").Cascade.All();
        }
    }

    public class ExameServerMapping : FluentNHibernate.Mapping.ClassMap<Dominio.Exames>
    {
        public ExameServerMapping()
        {
            Table("TB_Exame");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Codigo).Not.Nullable();
            Map(x => x.nomeExame).Length(50);
            Map(x => x.observacao).Length(255);
            References(x => x.clinica).Column("clinica_id").Cascade.All();
        }
    }
}
