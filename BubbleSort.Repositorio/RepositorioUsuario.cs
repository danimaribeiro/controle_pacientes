﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class RepositorioUsuario : RepositorioGenerico<Dominio.Usuario>, Dominio.Repositorio.IRepositorioUsuario
    {
        public RepositorioUsuario(NHibernate.ISession sessao) : base(sessao) { }
    }
}
