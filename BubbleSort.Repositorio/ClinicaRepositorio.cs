﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class ClinicaRepositorio : RepositorioGenerico<Dominio.Clinicas>, Dominio.Repositorio.IRepositorioClinica
    {
        public ClinicaRepositorio(NHibernate.ISession sessao) : base(sessao) { }
    }
}
