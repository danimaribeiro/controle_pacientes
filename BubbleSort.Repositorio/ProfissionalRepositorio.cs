﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class ProfissionalRepositorio : RepositorioGenerico<Dominio.Profissionais>, Dominio.Repositorio.IRepositorioProfissional
    {
        public ProfissionalRepositorio(NHibernate.ISession sessao) : base(sessao) { }
    }
}
