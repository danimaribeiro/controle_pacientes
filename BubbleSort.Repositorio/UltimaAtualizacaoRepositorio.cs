﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class UltimaAtualizacaoRepositorio : RepositorioGenerico<Dominio.Sincronizacao.UltimaAtualizacao>, Dominio.Repositorio.IRepositorioUltimaAtualizacao
    {
        public UltimaAtualizacaoRepositorio(NHibernate.ISession sessao) : base(sessao) { }
    }
}
