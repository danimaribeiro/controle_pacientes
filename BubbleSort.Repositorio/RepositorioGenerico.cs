﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public abstract class RepositorioGenerico<TEntidade> : Dominio.Repositorio.IRepositorio<TEntidade> where TEntidade : class, Dominio.IEntidade
    {
        protected virtual NHibernate.ISession SessionHibernate { get; set; }


        protected RepositorioGenerico(NHibernate.ISession sessao)
            : base()//IoC.GetInstance<Mecanica.Dominio.UoW.IUnitOfWork>())
        {
            this.SessionHibernate = sessao;
        }

        protected RepositorioGenerico(NHibernate.ISession sessao, Dominio.UoW.IUnitOfWork unitOfWork)
        {
            this.SessionHibernate = sessao;
            UnitOfWork = unitOfWork;
        }

        #region IRepositorio<TEntidade> Members

        private void AdicionarLog(TEntidade entidade, BubbleSort.Dominio.Enumeradores.TipoUpdate tipoatualizacao)
        {
            var registro = new BubbleSort.Dominio.Sincronizacao.RegistrosAtualizar();
            registro.DataAtualizacao = DateTime.Now;
            registro.TipoAtualizacao = tipoatualizacao;
            registro.uuid = entidade.Id;
            var tipo = entidade;
            if (tipo is BubbleSort.Dominio.Clinicas)
                registro.Tabela = "tb_clinica";
            else if (tipo is BubbleSort.Dominio.Consultas)
                registro.Tabela = "tb_consultas";
            else if (tipo is BubbleSort.Dominio.Convenios)
                registro.Tabela = "tb_convenio";
            else if (tipo is BubbleSort.Dominio.Endereco)
                registro.Tabela = "tb_endereco";
            else if (tipo is BubbleSort.Dominio.ExameConsulta)
                registro.Tabela = "tb_exames_Consulta";
            else if (tipo is BubbleSort.Dominio.Exames)
                registro.Tabela = "tb_exame";
            else if (tipo is BubbleSort.Dominio.Paciente)
                registro.Tabela = "tb_paciente";
            else if (tipo is BubbleSort.Dominio.Profissionais)
                registro.Tabela = "tb_profissional";
            else if (tipo is BubbleSort.Dominio.Usuario)
                registro.Tabela = "tb_usuario";

            new Repositorio.RepositorioRegistrosAtualizar(this.SessionHibernate).Adicionar(registro);
        }

        public Dominio.UoW.IUnitOfWork UnitOfWork { get; set; }

        public IEnumerable<TEntidade> Todos()
        {
            return this.SessionHibernate.QueryOver<TEntidade>().List<TEntidade>();
        }

        public IList<TEntidade> Todos(System.Linq.Expressions.Expression<Func<TEntidade, object>> path)
        {
            return this.SessionHibernate.QueryOver<TEntidade>().OrderBy(path).Asc.List<TEntidade>();
        }

        public IEnumerable<TEntidade> Todos(int pagina, int quantidade)
        {
            return this.SessionHibernate.QueryOver<TEntidade>()
                .OrderBy(x => x.Id).Desc.Take(quantidade).Skip((pagina - 1) * quantidade).List<TEntidade>();
        }

        public virtual string Adicionar(TEntidade entidade)
        {
            using (this.SessionHibernate.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                entidade.Codigo = this.SessionHibernate.CreateCriteria<TEntidade>().SetProjection(NHibernate.Criterion.Projections.Max("Codigo")).UniqueResult<long>() + 1;
                string id = (string)this.SessionHibernate.Save(entidade);
                AdicionarLog(entidade, Dominio.Enumeradores.TipoUpdate.Insert);
                this.SessionHibernate.Transaction.Commit();
                return id;
            }
        }

        public virtual string AdicionarSemLog(TEntidade entidade)
        {
            using (this.SessionHibernate.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {                
                string id = (string)this.SessionHibernate.Save(entidade);                
                this.SessionHibernate.Transaction.Commit();
                return id;
            }
        }

        public void Deletar(TEntidade entidade)
        {
            using (this.SessionHibernate.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                this.SessionHibernate.Delete(entidade);
                AdicionarLog(entidade, Dominio.Enumeradores.TipoUpdate.Delete);
                this.SessionHibernate.Transaction.Commit();
            }
        }

        public void Atualizar(TEntidade entidade)
        {
            using (this.SessionHibernate.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                this.SessionHibernate.Update(entidade);
                AdicionarLog(entidade, Dominio.Enumeradores.TipoUpdate.Update);
                this.SessionHibernate.Transaction.Commit();
            }
        }

        public void AtualizarSemLog(TEntidade entidade)
        {
            using (this.SessionHibernate.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
            {
                this.SessionHibernate.Update(entidade);                
                this.SessionHibernate.Transaction.Commit();
            }
        }


        public TEntidade BuscarPorId(string id)
        {   
            return this.SessionHibernate.QueryOver<TEntidade>().Where(x => x.Id == id).SingleOrDefault<TEntidade>();
        }

        public TEntidade CarregarPorId(string id)
        {
            return this.SessionHibernate.Get<TEntidade>(id);
        }

        public IEnumerable<TEntidade> BuscarPor(System.Linq.Expressions.Expression<Func<TEntidade, bool>> condicao)
        {
            return this.SessionHibernate.QueryOver<TEntidade>().Where(condicao).List<TEntidade>();
        }

        public IEnumerable<TEntidade> BuscarPor(System.Linq.Expressions.Expression<Func<TEntidade, bool>> condicao, System.Linq.Expressions.Expression<Func<TEntidade, object>> path)
        {
            return this.SessionHibernate.QueryOver<TEntidade>().Where(condicao).OrderBy(path).Asc.List<TEntidade>();
        }

        public TEntidade BuscarRegistro(System.Linq.Expressions.Expression<Func<TEntidade, bool>> condicao)
        {
            return this.SessionHibernate.QueryOver<TEntidade>().Where(condicao).SingleOrDefault<TEntidade>();
        }

        public long ProximoCodigo()
        {
            return this.SessionHibernate.CreateCriteria<TEntidade>().SetProjection(NHibernate.Criterion.Projections.Max("Codigo")).UniqueResult<long>() +1 ;
        }
      
        #endregion

        public void Dispose()
        {
            this.SessionHibernate.Close();
            this.SessionHibernate.Dispose();
        }


       
    }
}
