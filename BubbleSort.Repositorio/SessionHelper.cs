﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using FluentNHibernate.Automapping;
using NHibernate.Cfg;
using NHibernate.Cfg.Loquacious;
using NHibernate.Tool.hbm2ddl;

namespace BubbleSort.Repositorio
{
    public class SessionHelper
    {
        public string ConnectionString { get; set; }
        private static ISessionFactory _Factory;
        private static ISessionFactory _FactoryServer;

        private static ISession _Session;
        private static ISession _SessionServe;

        public static ISessionFactory FabricaSessao
        {
            get
            {
                lock (typeof(SessionHelper))
                {
                    if (_Factory == null)
                    {
                        new SessionHelper("local");
                    }
                }
                return _Factory;
            }
        }

        public static ISessionFactory FabricaSessaoServer
        {
            get
            {
                lock (typeof(SessionHelper))
                {
                    if (_FactoryServer == null)
                    {
                        new SessionHelper("server");
                    }
                }
                return _FactoryServer;
            }
        }

        public static ISession NovaSessao
        {
            get
            {
                if (_Session == null)
                {
                    _Session = FabricaSessao.OpenSession();
                }
                return _Session;
            }
        }

        public static ISession NovaSessaoServer
        {
            get
            {
                if (_SessionServe == null)
                {
                    _SessionServe = FabricaSessaoServer.OpenSession();
                }
                return _SessionServe;
            }
        }

        public SessionHelper(string tipo)
        {
            var cfg = BuildConfiguration(tipo);

            //var persistenceModel = BuildPersistenceModel();
            //persistenceModel.Configure(cfg);

            FluentNHibernate.Cfg.FluentConfiguration cfgFluent;
            if (tipo == "local")
            {
                cfgFluent = FluentNHibernate.Cfg.Fluently.Configure(cfg)
                    .Mappings(x => x.FluentMappings.Add<Mappings.ClinicaMapping>().
                        Add<Mappings.ConsultasMapping>().
                        Add<Mappings.ConvenioMapping>().
                        Add<Mappings.EnderecoMapping>().
                        Add<Mappings.ExameConsultaMapping>().
                        Add<Mappings.ExameMapping>().
                        Add<Mappings.PacienteMappings>().
                        Add<Mappings.ProfissionalMapping>().
                        Add<Mappings.RegistrosAtualizarMapping>().
                        Add<Mappings.UltimaAtualizacaoMapping>().
                        Add<Mappings.UsuarioMapping>());
            }
            else
            {
                cfgFluent = FluentNHibernate.Cfg.Fluently.Configure(cfg)
                    .Mappings(x => x.FluentMappings.Add<Mappings.ClinicaServerMapping>().
                        Add<Mappings.ConsultasServerMapping>().
                        Add<Mappings.ConvenioServerMapping>().
                        Add<Mappings.EnderecoServerMapping>().
                        Add<Mappings.ExameConsultaServerMapping>().
                        Add<Mappings.ExameServerMapping>().
                        Add<Mappings.PacienteServerMappings>().
                        Add<Mappings.ProfissionalServerMapping>().
                        Add<Mappings.RegistrosAtualizarMapping>().
                        Add<Mappings.UltimaAtualizacaoMapping>().
                        Add<Mappings.UsuarioServerMapping>());
            }

            cfg = cfgFluent.BuildConfiguration();


            if (tipo == "local")
                _Factory = BuildSessionFactory(cfg);
            else
                _FactoryServer = BuildSessionFactory(cfg);

            BuildSchema(cfg);
            //RegisterConponents(cfg, sessionFactory);
        }

        public Configuration BuildConfiguration(string tipo)
        {
            NHibernate.Cfg.Configuration configuracao;
            if (tipo == "local")
            {
                configuracao = new NHibernate.Cfg.Configuration();
                configuracao.DataBaseIntegration(x =>
                {
                    x.ConnectionStringName = "sqlCe";
                    x.Dialect<NHibernate.Dialect.MsSqlCe40Dialect>();
                    x.Driver<NHibernate.Driver.SqlServerCeDriver>();
                    x.LogFormatedSql = true;
                    x.LogSqlInConsole = true;
                })
                .Proxy(x => x.ProxyFactoryFactory<NHibernate.ByteCode.Castle.ProxyFactoryFactory>());

            }
            else
            {
                configuracao = new NHibernate.Cfg.Configuration();
                configuracao.DataBaseIntegration(x =>
                {
                    x.ConnectionStringName = "conexaoPostgres";
                    x.Dialect<NHibernate.Dialect.PostgreSQL82Dialect>();
                    x.Driver<NHibernate.Driver.NpgsqlDriver>();
                    x.LogFormatedSql = true;
                    x.LogSqlInConsole = true;
                })
                .Proxy(x => x.ProxyFactoryFactory<NHibernate.ByteCode.Castle.ProxyFactoryFactory>());
            }
            return configuracao;
        }

        public AutoPersistenceModel BuildPersistenceModel()
        {
            var persistenceModel = new AutoPersistenceModel();

            persistenceModel.ValidationEnabled = true;
            persistenceModel.AddMappingsFromAssembly(typeof(Dominio.IEntidade).Assembly);

            persistenceModel.WriteMappingsTo(@"./");

            return persistenceModel;
        }

        private ISessionFactory BuildSessionFactory(Configuration config)
        {
            var sessionFactory = config.BuildSessionFactory();

            if (sessionFactory == null)
                throw new Exception("Cannot build NHibernate Session Factory");

            return sessionFactory;
        }

        //public void RegisterConponents(Configuration config, ISessionFactory sessionFactory)
        //{
        //    builder.RegisterInstance(config).As<Configuration>().SingleInstance();
        //    builder.RegisterInstance(sessionFactory).As<ISessionFactory>().SingleInstance();
        //    builder.Register(x => x.Resolve<ISessionFactory>().OpenSession()).As<ISession>().InstancePerLifetimeScope();
        //    builder.Register(x => new UnitOfWork(x.Resolve<ISessionFactory>())).As<IUnitOfWork>();
        //    builder.Register(x => new UnitOfWorkFactory()).As<IUnitOfWorkFactory>().SingleInstance();
        //}

        private static void BuildSchema(Configuration config)
        {
            string caminhoScript = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "scriptBanco.sql");
            if (!System.IO.File.Exists(caminhoScript))
            {
                SchemaExport schema = new SchemaExport(config);

                using (System.IO.StreamWriter streamEscrita = new System.IO.StreamWriter(System.IO.File.Open(caminhoScript, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None)))
                {
                    schema.SetDelimiter(";");
                    schema.Execute(true, false, false, null, streamEscrita);
                }
            }
            //new SchemaExport(config).SetOutputFile(@"./Schema.sql").Create(true, true);
            //new SchemaExport(config).Execute(true, true, false);
        }

    }
}
