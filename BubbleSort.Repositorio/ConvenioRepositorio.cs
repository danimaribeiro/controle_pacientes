﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class ConvenioRepositorio : RepositorioGenerico<Dominio.Convenios>, Dominio.Repositorio.IRepositorioConvenio
    {
        public ConvenioRepositorio(NHibernate.ISession sessao) : base(sessao) { }
    }
}
