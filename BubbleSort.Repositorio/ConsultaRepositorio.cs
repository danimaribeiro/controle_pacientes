﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class ConsultaRepositorio : RepositorioGenerico<Dominio.Consultas>, Dominio.Repositorio.IRepositorioConsulta
    {
        public ConsultaRepositorio(NHibernate.ISession sessao) : base(sessao) { }
    }
}
