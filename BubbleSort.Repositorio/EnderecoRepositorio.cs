﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Repositorio
{
    public class EnderecoRepositorio : RepositorioGenerico<Dominio.Endereco>, Dominio.Repositorio.IRepositorioEndereco
    {
        public EnderecoRepositorio(NHibernate.ISession sessao) : base(sessao) { }
    }
}
