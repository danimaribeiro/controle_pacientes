﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using NHibernate;

namespace BubbleSort.Repositorio
{
    public class NHibernateHttpModule : System.Web.IHttpModule
    {

        public const string KEY = "_SESSION_NHIBERNATE_";

        private static ISession _session;
        public NHibernateHttpModule()
        {
        }

        #region "IHttpModule Members"

        public void Dispose()
        {
        }


        public void Init(HttpApplication context)
        {
            context.BeginRequest +=new EventHandler(context_BeginRequest);
            context.EndRequest +=new EventHandler(context_EndRequest);
        }

        #endregion

        private void context_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpContext context = application.Context;
            context.Items[KEY] = SessionHelper.NovaSessao;
        }

        private void context_EndRequest(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpContext context = application.Context;

            ISession session = context.Items[KEY] as ISession;
            if (session != null)
            {
                try
                {
                    session.Flush();
                    session.Close();
                }
                catch
                {
                }
            }
            context.Items[KEY] = null;
        }

        public static ISession CurrentSession
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    if (_session != null)
                    {
                        return _session;
                    }

                    _session = SessionHelper.NovaSessao;
                    return _session;
                }
                else
                {
                    HttpContext currentContext = HttpContext.Current;
                    ISession session = currentContext.Items[KEY] as ISession;
                    if (session == null)
                    {
                        session = SessionHelper.NovaSessao;
                        currentContext.Items[KEY] = session;
                    }
                    return session;
                }
            }
        }

    }

}
