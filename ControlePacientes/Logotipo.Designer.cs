﻿namespace ControlePacientes
{
    partial class Logotipo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Logotipo));
            this.pcbLogo = new System.Windows.Forms.PictureBox();
            this.btnSalvarLogo = new System.Windows.Forms.Button();
            this.btnSelecionarLogo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pcbLogo
            // 
            this.pcbLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbLogo.Location = new System.Drawing.Point(13, 13);
            this.pcbLogo.Name = "pcbLogo";
            this.pcbLogo.Size = new System.Drawing.Size(334, 143);
            this.pcbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbLogo.TabIndex = 0;
            this.pcbLogo.TabStop = false;
            // 
            // btnSalvarLogo
            // 
            this.btnSalvarLogo.Location = new System.Drawing.Point(224, 166);
            this.btnSalvarLogo.Name = "btnSalvarLogo";
            this.btnSalvarLogo.Size = new System.Drawing.Size(123, 23);
            this.btnSalvarLogo.TabIndex = 1;
            this.btnSalvarLogo.Text = "Salvar";
            this.btnSalvarLogo.UseVisualStyleBackColor = true;
            this.btnSalvarLogo.Click += new System.EventHandler(this.btnSalvarLogo_Click);
            // 
            // btnSelecionarLogo
            // 
            this.btnSelecionarLogo.Location = new System.Drawing.Point(13, 166);
            this.btnSelecionarLogo.Name = "btnSelecionarLogo";
            this.btnSelecionarLogo.Size = new System.Drawing.Size(123, 23);
            this.btnSelecionarLogo.TabIndex = 2;
            this.btnSelecionarLogo.Text = "Selecionar logo";
            this.btnSelecionarLogo.UseVisualStyleBackColor = true;
            this.btnSelecionarLogo.Click += new System.EventHandler(this.btnSelecionarLogo_Click);
            // 
            // Logotipo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 202);
            this.Controls.Add(this.btnSelecionarLogo);
            this.Controls.Add(this.btnSalvarLogo);
            this.Controls.Add(this.pcbLogo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Logotipo";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Logotipo";
            this.Load += new System.EventHandler(this.Logotipo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pcbLogo;
        private System.Windows.Forms.Button btnSalvarLogo;
        private System.Windows.Forms.Button btnSelecionarLogo;
    }
}