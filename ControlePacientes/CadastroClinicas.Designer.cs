﻿namespace ControlePacientes
{
    partial class CadastroClinicas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroClinicas));
            this.label1 = new System.Windows.Forms.Label();
            this.TxtNomeClinica = new System.Windows.Forms.TextBox();
            this.ctrNavigator1 = new ControlePacientes.Controles.CtrNavigator();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtNomeFantasia = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtEndereço = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtEnderecoNumero = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtBairro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCidade = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtEstado = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtInscricaoEstadual = new System.Windows.Forms.TextBox();
            this.TxtCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.TxtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtTelefoneFax = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtCep = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.DGClinicas = new System.Windows.Forms.DataGridView();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.LblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ColCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNomeFantasia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTelefoneClinica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColInscricaoEstadual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTelefoneFax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEndereco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DGClinicas)).BeginInit();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 15);
            this.label1.TabIndex = 28;
            this.label1.Text = "Nome da Clínica :";
            // 
            // TxtNomeClinica
            // 
            this.TxtNomeClinica.BackColor = System.Drawing.Color.White;
            this.TxtNomeClinica.Enabled = false;
            this.TxtNomeClinica.Location = new System.Drawing.Point(137, 38);
            this.TxtNomeClinica.Name = "TxtNomeClinica";
            this.TxtNomeClinica.Size = new System.Drawing.Size(414, 20);
            this.TxtNomeClinica.TabIndex = 0;
            // 
            // ctrNavigator1
            // 
            this.ctrNavigator1.DataSource = null;
            this.ctrNavigator1.Indice = -1;
            this.ctrNavigator1.Location = new System.Drawing.Point(33, 264);
            this.ctrNavigator1.Name = "ctrNavigator1";
            this.ctrNavigator1.Size = new System.Drawing.Size(547, 42);
            this.ctrNavigator1.TabIndex = 12;
            this.ctrNavigator1.MudaRegistroSelecionado += new ControlePacientes.Controles.CtrNavigator.MudaRegistro(this.ctrNavigator1_MudaRegistroSelecionado);
            this.ctrNavigator1.EventoNovo += new ControlePacientes.Controles.CtrNavigator.Novo(this.ctrNavigator1_EventoNovo);
            this.ctrNavigator1.CancelarAcao += new ControlePacientes.Controles.CtrNavigator.Cancelar(this.ctrNavigator1_CancelarAcao);
            this.ctrNavigator1.EditarRegistro += new ControlePacientes.Controles.CtrNavigator.Editar(this.ctrNavigator1_EditarRegistro);
            this.ctrNavigator1.SalvarRegistro += new ControlePacientes.Controles.CtrNavigator.Salvar(this.ctrNavigator1_SalvarRegistro);
            this.ctrNavigator1.ExcluirRegistro += new ControlePacientes.Controles.CtrNavigator.Excluir(this.ctrNavigator1_ExcluirRegistro);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 15);
            this.label2.TabIndex = 31;
            this.label2.Text = "Nome Fantasia :";
            // 
            // TxtNomeFantasia
            // 
            this.TxtNomeFantasia.BackColor = System.Drawing.Color.White;
            this.TxtNomeFantasia.Enabled = false;
            this.TxtNomeFantasia.Location = new System.Drawing.Point(137, 66);
            this.TxtNomeFantasia.Name = "TxtNomeFantasia";
            this.TxtNomeFantasia.Size = new System.Drawing.Size(414, 20);
            this.TxtNomeFantasia.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(64, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 33;
            this.label3.Text = "Endereço :";
            // 
            // TxtEndereço
            // 
            this.TxtEndereço.BackColor = System.Drawing.Color.White;
            this.TxtEndereço.Enabled = false;
            this.TxtEndereço.Location = new System.Drawing.Point(137, 150);
            this.TxtEndereço.Name = "TxtEndereço";
            this.TxtEndereço.Size = new System.Drawing.Size(414, 20);
            this.TxtEndereço.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(73, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 15);
            this.label4.TabIndex = 35;
            this.label4.Text = "Número :";
            // 
            // TxtEnderecoNumero
            // 
            this.TxtEnderecoNumero.BackColor = System.Drawing.Color.White;
            this.TxtEnderecoNumero.Enabled = false;
            this.TxtEnderecoNumero.Location = new System.Drawing.Point(137, 178);
            this.TxtEnderecoNumero.Name = "TxtEnderecoNumero";
            this.TxtEnderecoNumero.Size = new System.Drawing.Size(255, 20);
            this.TxtEnderecoNumero.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(83, 208);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 37;
            this.label5.Text = "Bairro :";
            // 
            // TxtBairro
            // 
            this.TxtBairro.BackColor = System.Drawing.Color.White;
            this.TxtBairro.Enabled = false;
            this.TxtBairro.Location = new System.Drawing.Point(137, 206);
            this.TxtBairro.Name = "TxtBairro";
            this.TxtBairro.Size = new System.Drawing.Size(414, 20);
            this.TxtBairro.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(79, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 15);
            this.label6.TabIndex = 39;
            this.label6.Text = "Cidade :";
            // 
            // TxtCidade
            // 
            this.TxtCidade.BackColor = System.Drawing.Color.White;
            this.TxtCidade.Enabled = false;
            this.TxtCidade.Location = new System.Drawing.Point(137, 234);
            this.TxtCidade.Name = "TxtCidade";
            this.TxtCidade.Size = new System.Drawing.Size(297, 20);
            this.TxtCidade.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(440, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 41;
            this.label7.Text = "Estado :";
            // 
            // TxtEstado
            // 
            this.TxtEstado.BackColor = System.Drawing.Color.White;
            this.TxtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtEstado.Enabled = false;
            this.TxtEstado.Location = new System.Drawing.Point(498, 234);
            this.TxtEstado.MaxLength = 2;
            this.TxtEstado.Name = "TxtEstado";
            this.TxtEstado.Size = new System.Drawing.Size(53, 20);
            this.TxtEstado.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(87, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 15);
            this.label8.TabIndex = 43;
            this.label8.Text = "CNPJ :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(274, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 15);
            this.label9.TabIndex = 45;
            this.label9.Text = "Inscrição Estadual :";
            // 
            // TxtInscricaoEstadual
            // 
            this.TxtInscricaoEstadual.BackColor = System.Drawing.Color.White;
            this.TxtInscricaoEstadual.Enabled = false;
            this.TxtInscricaoEstadual.Location = new System.Drawing.Point(402, 94);
            this.TxtInscricaoEstadual.Name = "TxtInscricaoEstadual";
            this.TxtInscricaoEstadual.Size = new System.Drawing.Size(149, 20);
            this.TxtInscricaoEstadual.TabIndex = 3;
            // 
            // TxtCNPJ
            // 
            this.TxtCNPJ.Location = new System.Drawing.Point(137, 94);
            this.TxtCNPJ.Mask = "00.000.000/0000-00";
            this.TxtCNPJ.Name = "TxtCNPJ";
            this.TxtCNPJ.Size = new System.Drawing.Size(131, 20);
            this.TxtCNPJ.TabIndex = 2;
            // 
            // TxtTelefone
            // 
            this.TxtTelefone.Location = new System.Drawing.Point(137, 122);
            this.TxtTelefone.Mask = "(00)0000-0000";
            this.TxtTelefone.Name = "TxtTelefone";
            this.TxtTelefone.Size = new System.Drawing.Size(131, 20);
            this.TxtTelefone.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(70, 124);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 15);
            this.label10.TabIndex = 47;
            this.label10.Text = "Telefone :";
            // 
            // TxtTelefoneFax
            // 
            this.TxtTelefoneFax.Location = new System.Drawing.Point(402, 122);
            this.TxtTelefoneFax.Mask = "(00)0000-0000";
            this.TxtTelefoneFax.Name = "TxtTelefoneFax";
            this.TxtTelefoneFax.Size = new System.Drawing.Size(149, 20);
            this.TxtTelefoneFax.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(359, 124);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 15);
            this.label11.TabIndex = 49;
            this.label11.Text = "Fax :";
            // 
            // TxtCep
            // 
            this.TxtCep.Location = new System.Drawing.Point(443, 178);
            this.TxtCep.Mask = "00.000-000";
            this.TxtCep.Name = "TxtCep";
            this.TxtCep.Size = new System.Drawing.Size(108, 20);
            this.TxtCep.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(399, 180);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 15);
            this.label12.TabIndex = 51;
            this.label12.Text = "Cep :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(79, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 15);
            this.label13.TabIndex = 53;
            this.label13.Text = "Código :";
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.White;
            this.txtCodigo.Enabled = false;
            this.txtCodigo.Location = new System.Drawing.Point(137, 10);
            this.txtCodigo.MaxLength = 2;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Size = new System.Drawing.Size(131, 20);
            this.txtCodigo.TabIndex = 0;
            // 
            // DGClinicas
            // 
            this.DGClinicas.AllowUserToAddRows = false;
            this.DGClinicas.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DGClinicas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGClinicas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DGClinicas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGClinicas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCodigo,
            this.ColNome,
            this.ColNomeFantasia,
            this.ColTelefoneClinica,
            this.ColCNPJ,
            this.ColInscricaoEstadual,
            this.ColTelefoneFax,
            this.ColEndereco});
            this.DGClinicas.Location = new System.Drawing.Point(12, 319);
            this.DGClinicas.MultiSelect = false;
            this.DGClinicas.Name = "DGClinicas";
            this.DGClinicas.ReadOnly = true;
            this.DGClinicas.RowHeadersWidth = 15;
            this.DGClinicas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGClinicas.Size = new System.Drawing.Size(733, 219);
            this.DGClinicas.TabIndex = 14;
            this.DGClinicas.SelectionChanged += new System.EventHandler(this.DGClinicas_SelectionChanged);
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblStatus});
            this.StatusStrip.Location = new System.Drawing.Point(0, 552);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(768, 22);
            this.StatusStrip.TabIndex = 55;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // LblStatus
            // 
            this.LblStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(124, 17);
            this.LblStatus.Text = "Status : Consultando";
            // 
            // ColCodigo
            // 
            this.ColCodigo.DataPropertyName = "Codigo";
            this.ColCodigo.HeaderText = "Código";
            this.ColCodigo.Name = "ColCodigo";
            this.ColCodigo.ReadOnly = true;
            this.ColCodigo.Width = 60;
            // 
            // ColNome
            // 
            this.ColNome.DataPropertyName = "Nome";
            this.ColNome.HeaderText = "Nome Clínica";
            this.ColNome.Name = "ColNome";
            this.ColNome.ReadOnly = true;
            this.ColNome.Width = 190;
            // 
            // ColNomeFantasia
            // 
            this.ColNomeFantasia.DataPropertyName = "NomeFantasia";
            this.ColNomeFantasia.HeaderText = "Nome Fantasia";
            this.ColNomeFantasia.Name = "ColNomeFantasia";
            this.ColNomeFantasia.ReadOnly = true;
            this.ColNomeFantasia.Width = 190;
            // 
            // ColTelefoneClinica
            // 
            this.ColTelefoneClinica.DataPropertyName = "TelefoneClinica";
            this.ColTelefoneClinica.HeaderText = "Telefone";
            this.ColTelefoneClinica.Name = "ColTelefoneClinica";
            this.ColTelefoneClinica.ReadOnly = true;
            // 
            // ColCNPJ
            // 
            this.ColCNPJ.DataPropertyName = "CNPJ";
            this.ColCNPJ.HeaderText = "CNPJ";
            this.ColCNPJ.Name = "ColCNPJ";
            this.ColCNPJ.ReadOnly = true;
            this.ColCNPJ.Width = 40;
            // 
            // ColInscricaoEstadual
            // 
            this.ColInscricaoEstadual.DataPropertyName = "InscricaoEstadual";
            this.ColInscricaoEstadual.HeaderText = "Inscrição Estadual";
            this.ColInscricaoEstadual.Name = "ColInscricaoEstadual";
            this.ColInscricaoEstadual.ReadOnly = true;
            this.ColInscricaoEstadual.Width = 160;
            // 
            // ColTelefoneFax
            // 
            this.ColTelefoneFax.DataPropertyName = "TelefoneFax";
            this.ColTelefoneFax.HeaderText = "Fax";
            this.ColTelefoneFax.Name = "ColTelefoneFax";
            this.ColTelefoneFax.ReadOnly = true;
            // 
            // ColEndereco
            // 
            this.ColEndereco.DataPropertyName = "Endereco";
            this.ColEndereco.HeaderText = "Endereço";
            this.ColEndereco.Name = "ColEndereco";
            this.ColEndereco.ReadOnly = true;
            this.ColEndereco.Visible = false;
            // 
            // CadastroClinicas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 574);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.DGClinicas);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.TxtCep);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TxtTelefoneFax);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TxtTelefone);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TxtCNPJ);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TxtInscricaoEstadual);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtEstado);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtCidade);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtBairro);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtEnderecoNumero);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtEndereço);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtNomeFantasia);
            this.Controls.Add(this.ctrNavigator1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtNomeClinica);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastroClinicas";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Clínicas";
            this.Load += new System.EventHandler(this.CadastroClinicas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGClinicas)).EndInit();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtNomeClinica;
        private Controles.CtrNavigator ctrNavigator1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtNomeFantasia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtEndereço;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtEnderecoNumero;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtBairro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtCidade;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtEstado;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtInscricaoEstadual;
        private System.Windows.Forms.MaskedTextBox TxtCNPJ;
        private System.Windows.Forms.MaskedTextBox TxtTelefone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox TxtTelefoneFax;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox TxtCep;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.DataGridView DGClinicas;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel LblStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNome;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNomeFantasia;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTelefoneClinica;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCNPJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColInscricaoEstadual;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTelefoneFax;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEndereco;
    }
}