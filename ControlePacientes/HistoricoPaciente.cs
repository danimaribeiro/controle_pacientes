﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes
{
    public partial class HistoricoPaciente : Form
    {
        private BubbleSort.Dominio.Paciente _Paciente;
        private BubbleSort.Repositorio.ConsultaRepositorio _RepositorioConsulta;
        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;

        public HistoricoPaciente(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session, BubbleSort.Dominio.Paciente paciente)
        {
            InitializeComponent();
            _Paciente = paciente;
            _TratadorDeErro = tratadorErro;
            _RepositorioConsulta = new BubbleSort.Repositorio.ConsultaRepositorio(session);
        }

        private void HistoricoPaciente_Load(object sender, EventArgs e)
        {
            var consultas = _RepositorioConsulta.BuscarPor(x => x.Paciente.Id == _Paciente.Id && x.status == 1);
            foreach (var item in consultas)
            {
                flowLayoutPanel1.Controls.Add(new Controles.VisualizarConsulta(item));
            }
        }
    }
}
