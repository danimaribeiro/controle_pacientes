﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes
{
    public partial class NovaConsulta : Form
    {
        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private NHibernate.ISession _Session;

        private BubbleSort.Repositorio.ExameRepositorio _RepositorioExame;
        private BubbleSort.Repositorio.ClinicaRepositorio _RepositorioClinica;
        private BubbleSort.Repositorio.ProfissionalRepositorio _RepositorioProfissionais;
        private BubbleSort.Repositorio.ConsultaRepositorio _ConsultaRepositorio;

        private BubbleSort.Dominio.Paciente _Paciente;
        private BubbleSort.Dominio.Consultas _Consulta;

        private BindingSource _BindingExames;

        private BubbleSort.Dominio.ExameConsulta _ExameAtualizando;

        public NovaConsulta(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session, BubbleSort.Dominio.Paciente paciente)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Session = session;
            _Paciente = paciente;
            _RepositorioExame = new BubbleSort.Repositorio.ExameRepositorio(session);
            _RepositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(session);
            _RepositorioProfissionais = new BubbleSort.Repositorio.ProfissionalRepositorio(session);
            _ConsultaRepositorio = new BubbleSort.Repositorio.ConsultaRepositorio(session);
        }

        public NovaConsulta(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session, BubbleSort.Dominio.Paciente paciente, BubbleSort.Dominio.Consultas consulta)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Session = session;
            _Paciente = paciente;
            _Consulta = consulta;
            _RepositorioExame = new BubbleSort.Repositorio.ExameRepositorio(session);
            _RepositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(session);
            _RepositorioProfissionais = new BubbleSort.Repositorio.ProfissionalRepositorio(session);
            _ConsultaRepositorio = new BubbleSort.Repositorio.ConsultaRepositorio(session);
        }



        private void NovaConsulta_Load(object sender, EventArgs e)
        {
            CarregarClinicas();
            CarregarExames();
            CarregarProfissionais();
            CarregarDados();
        }

        private void btnAddExame_Click(object sender, EventArgs e)
        {
            new CadastroExames(_TratadorDeErro, _Session).ShowDialog();
        }

        private void CarregarExames()
        {
            var todos = _RepositorioExame.Todos(x => x.nomeExame);
            cmbExame.DataSource = todos;
        }

        private void CarregarClinicas()
        {
            var todos = _RepositorioClinica.Todos(x => x.Nome);
            cmbClinica.DataSource = todos;
        }

        private void CarregarProfissionais()
        {
            var todos = _RepositorioProfissionais.Todos(x => x.Nome);
            cmbProfissional.DataSource = todos;
        }

        private void CarregarDados()
        {
            gdvExames.AutoGenerateColumns = false;
            TxtNome.Text = _Paciente.Nome;
            TxtTelefone.Text = _Paciente.TelefoneCelular;
            if (_Paciente.Clinica != null)
                cmbClinica.SelectedItem = _Paciente.Clinica;
            if (_Paciente.Profissional != null)
                cmbProfissional.SelectedItem = _Paciente.Profissional;

            if (_Consulta == null)
            {
                ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Inserindo);
                this.CamposInterface(Controles.CtrNavigator.Status.Inserindo);
            }
            else
            {
                ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                _BindingExames = new BindingSource(_Consulta.ListaExames, "");
                gdvExames.DataSource = _BindingExames;
                this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
            }
            numericHora.Value = DateTime.Now.Hour;
            numericMinuto.Value = DateTime.Now.Minute;
        }

        private void EnabledCampos(bool enabled)
        {
            foreach (Control ctr in this.tabPage1.Controls)
            {
                if (ctr is ComboBox)
                    ((ComboBox)ctr).Enabled = enabled;
                else if (ctr is TextBox)
                    ((TextBox)ctr).Enabled = enabled;
                else if (ctr is MaskedTextBox)
                    ((MaskedTextBox)ctr).Enabled = enabled;
                else if (ctr is Button)
                    ((Button)ctr).Enabled = enabled;
                else if (ctr is DateTimePicker)
                    ((DateTimePicker)ctr).Enabled = enabled;
                else if (ctr is RichTextBox)
                    ((RichTextBox)ctr).Enabled = enabled;
            }
            foreach (Control ctr in this.groupBox1.Controls)
            {
                if (ctr is ComboBox)
                    ((ComboBox)ctr).Enabled = enabled;
                else if (ctr is TextBox)
                    ((TextBox)ctr).Enabled = enabled;
                else if (ctr is MaskedTextBox)
                    ((MaskedTextBox)ctr).Enabled = enabled;
                else if (ctr is Button)
                    ((Button)ctr).Enabled = enabled;
                else if (ctr is DateTimePicker)
                    ((DateTimePicker)ctr).Enabled = enabled;
                else if (ctr is RichTextBox)
                    ((RichTextBox)ctr).Enabled = enabled;
            }
        }

        private void ctrNavigator1_EventoNovo()
        {
            _Consulta = null;
            _Consulta = new BubbleSort.Dominio.Consultas();
            this.CamposInterface(Controles.CtrNavigator.Status.Inserindo);
        }

        private void ctrNavigator1_ExcluirRegistro(object objExcluir)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Excluindo);
            MessageBox.Show("Não é possível excluir uma consulta! Apenas coloque o estado dela para finalizado!");
            //if (MessageBox.Show("Deseja excluir o registro.", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    try
            //    {
            //        //_re.Deletar(_Paciente);                    
            //    } catch (Exception ex)
            //    {
            //        this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            //    }
            //}
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void ctrNavigator1_SalvarRegistro(object objSalvar)
        {
            try
            {
                this.ValidaDados();
                if (string.IsNullOrWhiteSpace(_Consulta.Id))
                {
                    _ConsultaRepositorio.Adicionar(_Consulta);
                }
                else
                    _ConsultaRepositorio.Atualizar(_Consulta);
                ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
            } catch (Exception ex)
            {
                this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        private void ValidaDados()
        {
            if (string.IsNullOrWhiteSpace(txtObservacaoConsulta.Text))
                throw new Exception("A observação é obrigatória");
            _Consulta.Data = datePickerConsulta.Value.Date.AddHours((double)numericHora.Value).AddMinutes((double)numericMinuto.Value);
            _Consulta.Observacoes = txtObservacaoConsulta.Rtf;
            _Consulta.status = cmbStatus.SelectedIndex;
            _Consulta.Clinica = (BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem;
            _Consulta.Tipo = (BubbleSort.Dominio.Enumeradores.TipoConsulta)cmbTipo.SelectedIndex;
            _Consulta.Paciente = _Paciente;
        }

        private void CamposInterface(Controles.CtrNavigator.Status status)
        {
            if (_Consulta == null)
            {
                _Consulta = new BubbleSort.Dominio.Consultas();
                _Consulta.ListaExames = new List<BubbleSort.Dominio.ExameConsulta>();
                _BindingExames = new BindingSource(_Consulta.ListaExames, "");
                gdvExames.DataSource = _BindingExames;
            }

            if (_Consulta.Data != DateTime.MinValue)
            {
                datePickerConsulta.Value = _Consulta.Data;
                numericHora.Value = _Consulta.Data.Hour;
                numericMinuto.Value = _Consulta.Data.Minute;
            }
            cmbTipo.SelectedIndex = (int)_Consulta.Tipo;
            cmbClinica.SelectedItem = _Consulta.Clinica;
            txtObservacaoConsulta.Rtf = _Consulta.Observacoes;
            cmbStatus.SelectedIndex = _Consulta.status;
            if (_BindingExames != null)
                _BindingExames.ResetBindings(true);

            if (status == Controles.CtrNavigator.Status.Inserindo)
            {
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Inserindo";
            }
            else if (status == Controles.CtrNavigator.Status.Editando)
            {
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Editando";
            }
            else if (status == Controles.CtrNavigator.Status.Excluindo)
            {
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Excluindo";
            }
            else
            {
                this.ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Consultando";
            }
        }

        private void BtnSalvar_Click(object sender, EventArgs e)
        {
            if (cmbExame.SelectedItem != null)
            {
                if (_Consulta == null)
                {
                    _Consulta = new BubbleSort.Dominio.Consultas();
                    _Consulta.ListaExames = new List<BubbleSort.Dominio.ExameConsulta>();
                    _BindingExames = new BindingSource(_Consulta.ListaExames, "");
                    gdvExames.DataSource = _BindingExames;
                }
                BubbleSort.Dominio.ExameConsulta exame;
                if (_ExameAtualizando != null)
                    exame = _ExameAtualizando;
                else
                    exame = new BubbleSort.Dominio.ExameConsulta();
                exame.Consulta = _Consulta;
                exame.Exame = (BubbleSort.Dominio.Exames)cmbExame.SelectedItem;
                exame.Observacao = txtObservacaoExame.Text;
                _Consulta.ListaExames.Remove(exame);
                _Consulta.ListaExames.Add(exame);
                _BindingExames.ResetBindings(true);
                BtnCancelar.Enabled = false;
            }
            else
                MessageBox.Show("Nenhum exame cadastrado.");
        }

        private void CamposExames()
        {
            if (_ExameAtualizando != null)
            {
                cmbExame.SelectedItem = _ExameAtualizando.Exame;
                txtObservacaoExame.Text = _ExameAtualizando.Observacao;
                BtnCancelar.Enabled = true;
            }
        }

        private void ctrNavigator1_EditarRegistro(object objEditar)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Editando);
        }

        private void ctrNavigator1_CancelarAcao()
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void txtObservacaoExame_TextChanged(object sender, EventArgs e)
        {
            this.AtualizaCaracteresRestantes(txtObservacaoExame, lblCaracteresRestantesExame);
        }

        private void AtualizaCaracteresRestantes(RichTextBox txtbox, Label lbl)
        {
            lbl.Text = (txtbox.MaxLength - txtbox.Text.Length) + " caracteres restantes.";
        }

        private void AtualizaCaracteresRestantes(TextBox txtbox, Label lbl)
        {
            lbl.Text = (txtbox.MaxLength - txtbox.Text.Length) + " caracteres restantes.";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtObservacaoConsulta.SelectionColor = colorDialog1.Color;
        }

        private void btnFonte_Click(object sender, EventArgs e)
        {
            if (fontDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtObservacaoConsulta.SelectionFont = fontDialog1.Font;
        }

        private void gdvExames_SelectionChanged(object sender, EventArgs e)
        {
            if (this.gdvExames.Rows.Count > 0)
            {
                if (gdvExames.SelectedRows.Count > 0)
                {
                    _ExameAtualizando = (BubbleSort.Dominio.ExameConsulta)gdvExames.SelectedRows[0].DataBoundItem;
                    if (ctrNavigator1.DataSource != null)
                        ctrNavigator1.Indice = gdvExames.SelectedRows[0].Index;
                }
            }
        }

        private void BtnCancelar_Click_1(object sender, EventArgs e)
        {
            BtnCancelar.Enabled = false;
            cmbExame.SelectedIndex = -1;
            txtObservacaoExame.Text = "";
            _ExameAtualizando = null;
        }

        private void gdvExames_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                _ExameAtualizando = (BubbleSort.Dominio.ExameConsulta)gdvExames.Rows[e.RowIndex].DataBoundItem;
                CamposExames();
            }
            else if (e.ColumnIndex == 4)
            {
                if (MessageBox.Show("Deseja excluir o registro.", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var exame = (BubbleSort.Dominio.ExameConsulta)gdvExames.Rows[e.RowIndex].DataBoundItem;
                    _Consulta.ListaExames.Remove(exame);
                    _BindingExames.ResetBindings(true);
                }
            }
        }


    }
}
