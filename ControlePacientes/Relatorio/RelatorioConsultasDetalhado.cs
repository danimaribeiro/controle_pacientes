﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace ControlePacientes.Relatorio
{
    public partial class RelatorioConsultasDetalhado : Form
    {
        public RelatorioConsultasDetalhado(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Repositorio = new BubbleSort.Repositorio.ConsultaRepositorio(session);
            _RepositorioPaciente = new BubbleSort.Repositorio.PacienteRepositorio(session);
            _RepositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(session);
        }

        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private BubbleSort.Repositorio.ConsultaRepositorio _Repositorio;
        private BubbleSort.Repositorio.PacienteRepositorio _RepositorioPaciente;
        private BubbleSort.Repositorio.ClinicaRepositorio _RepositorioClinica;

        private void RelatorioConsultas_Load(object sender, EventArgs e)
        {
            try
            {
                this.PreencherCombos();
            }
            catch (Exception ex) { _TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica); }
        }

        private void PreencherCombos()
        {
            List<BubbleSort.Dominio.Clinicas> listaClinica = (List<BubbleSort.Dominio.Clinicas>)_RepositorioClinica.Todos();
            listaClinica.Insert(0, new BubbleSort.Dominio.Clinicas() { Id = "", Nome = "Todas" });
            this.cmbClinica.DataSource = listaClinica;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            if (cmbConsulta.SelectedIndex >= 0)
            {
                try
                {
                    BubbleSort.Dominio.Consultas consulta = ((BubbleSort.Dominio.Consultas)cmbConsulta.SelectedItem);//_Repositorio.BuscarPor(x => x.Id == ((BubbleSort.Dominio.Consultas)cmbConsulta.SelectedItem).Id);
                    DsDetalhesConsulta ds = new DsDetalhesConsulta();
                    if (consulta.ListaExames.Count == 0)
                        ds.TB_Consulta_Detalhes.AddTB_Consulta_DetalhesRow(consulta.NomePaciente, consulta.Paciente.Cpf, consulta.Paciente.Email, consulta.Paciente.TelefoneFixo, consulta.Paciente.TelefoneCelular,
                            consulta.Paciente.NomeConvenio, consulta.Paciente.Endereco.Rua + " - " + consulta.Paciente.Endereco.Numero, consulta.Paciente.Endereco.Bairro, consulta.Paciente.Endereco.Cidade, consulta.Paciente.Endereco.Cep,
                            consulta.NomeProfissional, consulta.NomeClinica, consulta.Clinica.TelefoneClinica, consulta.Clinica.TelefoneFax, "", "");
                    else
                        for (int i = 0; i < consulta.ListaExames.Count; i++)
                            ds.TB_Consulta_Detalhes.AddTB_Consulta_DetalhesRow(consulta.NomePaciente, consulta.Paciente.Cpf, consulta.Paciente.Email, consulta.Paciente.TelefoneFixo, consulta.Paciente.TelefoneCelular,
                                consulta.Paciente.NomeConvenio, consulta.Paciente.Endereco.Rua + " - " + consulta.Paciente.Endereco.Numero, consulta.Paciente.Endereco.Bairro, consulta.Paciente.Endereco.Cidade, consulta.Paciente.Endereco.Cep,
                                consulta.NomeProfissional, consulta.NomeClinica, consulta.Clinica.TelefoneClinica, consulta.Clinica.TelefoneFax, consulta.ListaExames[i].NomeExame, consulta.ListaExames[i].Observacao);

                    reportConsultas.LocalReport.EnableExternalImages = true;
                    ReportParameterCollection parameters = new ReportParameterCollection();
                    string LogoPath = "file://" + System.IO.Path.Combine(Application.StartupPath, @"Img\Logo.jpg");
                    parameters.Add(new ReportParameter("parImgLogo", LogoPath));
                    reportConsultas.LocalReport.SetParameters(parameters); 

                    this.DsDetalhesConsultaBindingSource.DataSource = ds;
                    this.reportConsultas.RefreshReport();
                }
                catch (Exception ex)
                {
                    _TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
                }
            }
            else MessageBox.Show("Primeiro você deve selecionar a consulta.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                System.Linq.Expressions.Expression<Func<BubbleSort.Dominio.Consultas, bool>> condicao = null;
                DateTime data = new DateTime(cmbDataInicial.Value.Year, cmbDataInicial.Value.Month, cmbDataInicial.Value.Day);
                if (((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id == "")
                    condicao = (x => x.Data > data && x.Data < data.AddDays(1));
                else
                    condicao = (x => x.Data > data && x.Data < data.AddDays(1) && x.Clinica.Id == ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id);
                var lista = _Repositorio.BuscarPor(condicao);
                this.cmbConsulta.DataSource = lista;
            }
            catch (Exception ex) { _TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica); }
        }
    }
}
