﻿namespace ControlePacientes.Relatorio
{
    partial class RelatorioConsultasDetalhado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RelatorioConsultasDetalhado));
            this.DsDetalhesConsultaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbClinica = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cmbDataInicial = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.reportConsultas = new Microsoft.Reporting.WinForms.ReportViewer();
            this.btnOk = new System.Windows.Forms.Button();
            this.cmbConsulta = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DsDetalhesConsultaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // DsDetalhesConsultaBindingSource
            // 
            this.DsDetalhesConsultaBindingSource.DataMember = "TB_Consulta_Detalhes";
            this.DsDetalhesConsultaBindingSource.DataSource = typeof(ControlePacientes.Relatorio.DsDetalhesConsulta);
            // 
            // cmbClinica
            // 
            this.cmbClinica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClinica.FormattingEnabled = true;
            this.cmbClinica.Location = new System.Drawing.Point(90, 12);
            this.cmbClinica.Name = "cmbClinica";
            this.cmbClinica.Size = new System.Drawing.Size(261, 21);
            this.cmbClinica.TabIndex = 125;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 126;
            this.label1.Text = "Clinica :";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Image = global::ControlePacientes.Properties.Resources.apply;
            this.btnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultar.Location = new System.Drawing.Point(628, 39);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(155, 23);
            this.btnConsultar.TabIndex = 130;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cmbDataInicial
            // 
            this.cmbDataInicial.Location = new System.Drawing.Point(417, 11);
            this.cmbDataInicial.Name = "cmbDataInicial";
            this.cmbDataInicial.Size = new System.Drawing.Size(261, 20);
            this.cmbDataInicial.TabIndex = 131;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(370, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 15);
            this.label2.TabIndex = 132;
            this.label2.Text = "Data :";
            // 
            // reportConsultas
            // 
            this.reportConsultas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "DsConsultaDetalhes";
            reportDataSource1.Value = this.DsDetalhesConsultaBindingSource;
            this.reportConsultas.LocalReport.DataSources.Add(reportDataSource1);
            this.reportConsultas.LocalReport.ReportEmbeddedResource = "ControlePacientes.Relatorio.ReportConsultaDetalhes.rdlc";
            this.reportConsultas.Location = new System.Drawing.Point(15, 68);
            this.reportConsultas.Name = "reportConsultas";
            this.reportConsultas.Size = new System.Drawing.Size(768, 320);
            this.reportConsultas.TabIndex = 135;
            // 
            // btnOk
            // 
            this.btnOk.Image = global::ControlePacientes.Properties.Resources.apply;
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(712, 10);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(71, 23);
            this.btnOk.TabIndex = 136;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // cmbConsulta
            // 
            this.cmbConsulta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConsulta.FormattingEnabled = true;
            this.cmbConsulta.Location = new System.Drawing.Point(149, 41);
            this.cmbConsulta.Name = "cmbConsulta";
            this.cmbConsulta.Size = new System.Drawing.Size(407, 21);
            this.cmbConsulta.TabIndex = 137;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 15);
            this.label4.TabIndex = 138;
            this.label4.Text = "Selecione a consulta :";
            // 
            // RelatorioConsultasDetalhado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 400);
            this.Controls.Add(this.cmbConsulta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.reportConsultas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbDataInicial);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.cmbClinica);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RelatorioConsultasDetalhado";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Relatório detalhado de consulta";
            this.Load += new System.EventHandler(this.RelatorioConsultas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DsDetalhesConsultaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbClinica;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.DateTimePicker cmbDataInicial;
        private System.Windows.Forms.Label label2;
        private Microsoft.Reporting.WinForms.ReportViewer reportConsultas;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ComboBox cmbConsulta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource DsDetalhesConsultaBindingSource;
    }
}