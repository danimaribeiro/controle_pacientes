﻿namespace ControlePacientes.Relatorio
{
    partial class RelatorioConsultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RelatorioConsultas));
            this.ConsultasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportDetalhesConsulta = new Microsoft.Reporting.WinForms.ReportViewer();
            this.cmbDataFinal = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDataInicial = new System.Windows.Forms.DateTimePicker();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cmbClinica = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbPaciente = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ConsultasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ConsultasBindingSource
            // 
            this.ConsultasBindingSource.DataSource = typeof(BubbleSort.Dominio.Consultas);
            // 
            // reportDetalhesConsulta
            // 
            this.reportDetalhesConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "DsConsulta";
            reportDataSource1.Value = this.ConsultasBindingSource;
            this.reportDetalhesConsulta.LocalReport.DataSources.Add(reportDataSource1);
            this.reportDetalhesConsulta.LocalReport.ReportEmbeddedResource = "ControlePacientes.Relatorio.ReportConsulta.rdlc";
            this.reportDetalhesConsulta.Location = new System.Drawing.Point(12, 101);
            this.reportDetalhesConsulta.Name = "reportDetalhesConsulta";
            this.reportDetalhesConsulta.Size = new System.Drawing.Size(771, 325);
            this.reportDetalhesConsulta.TabIndex = 0;
            // 
            // cmbDataFinal
            // 
            this.cmbDataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.cmbDataFinal.Location = new System.Drawing.Point(367, 57);
            this.cmbDataFinal.Name = "cmbDataFinal";
            this.cmbDataFinal.Size = new System.Drawing.Size(190, 20);
            this.cmbDataFinal.TabIndex = 143;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(299, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 15);
            this.label3.TabIndex = 142;
            this.label3.Text = "Data Fim :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 15);
            this.label2.TabIndex = 141;
            this.label2.Text = "Data Início :";
            // 
            // cmbDataInicial
            // 
            this.cmbDataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.cmbDataInicial.Location = new System.Drawing.Point(104, 57);
            this.cmbDataInicial.Name = "cmbDataInicial";
            this.cmbDataInicial.Size = new System.Drawing.Size(190, 20);
            this.cmbDataInicial.TabIndex = 140;
            // 
            // btnConsultar
            // 
            this.btnConsultar.Image = global::ControlePacientes.Properties.Resources.apply;
            this.btnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultar.Location = new System.Drawing.Point(586, 55);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(155, 23);
            this.btnConsultar.TabIndex = 139;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cmbClinica
            // 
            this.cmbClinica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClinica.FormattingEnabled = true;
            this.cmbClinica.Location = new System.Drawing.Point(465, 24);
            this.cmbClinica.Name = "cmbClinica";
            this.cmbClinica.Size = new System.Drawing.Size(276, 21);
            this.cmbClinica.TabIndex = 137;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(408, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 138;
            this.label1.Text = "Clinica :";
            // 
            // cmbPaciente
            // 
            this.cmbPaciente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPaciente.FormattingEnabled = true;
            this.cmbPaciente.Location = new System.Drawing.Point(83, 24);
            this.cmbPaciente.Name = "cmbPaciente";
            this.cmbPaciente.Size = new System.Drawing.Size(278, 21);
            this.cmbPaciente.TabIndex = 135;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(26, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 15);
            this.label9.TabIndex = 136;
            this.label9.Text = "Cliente :";
            // 
            // RelatorioConsultas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 438);
            this.Controls.Add(this.cmbDataFinal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbDataInicial);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.cmbClinica);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbPaciente);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.reportDetalhesConsulta);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RelatorioConsultas";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Relatório de consultas";
            this.Load += new System.EventHandler(this.RelatorioConsultaDetalhes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ConsultasBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportDetalhesConsulta;
        private System.Windows.Forms.DateTimePicker cmbDataFinal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker cmbDataInicial;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.ComboBox cmbClinica;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbPaciente;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.BindingSource ConsultasBindingSource;
    }
}