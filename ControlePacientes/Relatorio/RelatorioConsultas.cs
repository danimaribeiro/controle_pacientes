﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace ControlePacientes.Relatorio
{
    public partial class RelatorioConsultas : Form
    {
        public RelatorioConsultas(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Repositorio = new BubbleSort.Repositorio.ConsultaRepositorio(session);
            _RepositorioPaciente = new BubbleSort.Repositorio.PacienteRepositorio(session);
            _RepositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(session);
        }

        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private BubbleSort.Repositorio.ConsultaRepositorio _Repositorio;
        private BubbleSort.Repositorio.PacienteRepositorio _RepositorioPaciente;
        private BubbleSort.Repositorio.ClinicaRepositorio _RepositorioClinica;

        private void PreencherCombos()
        {
            List<BubbleSort.Dominio.Paciente> listaPacientes = (List<BubbleSort.Dominio.Paciente>)_RepositorioPaciente.Todos();
            listaPacientes.Insert(0, new BubbleSort.Dominio.Paciente() { Id = "", Nome = "Todos" });
            this.cmbPaciente.DataSource = listaPacientes;

            List<BubbleSort.Dominio.Clinicas> listaClinica = (List<BubbleSort.Dominio.Clinicas>)_RepositorioClinica.Todos();
            listaClinica.Insert(0, new BubbleSort.Dominio.Clinicas() { Id = "", Nome = "Todas" });
            this.cmbClinica.DataSource = listaClinica;
        }

        private void RelatorioConsultaDetalhes_Load(object sender, EventArgs e)
        {
            try
            {
                this.PreencherCombos();
            }
            catch (Exception ex) { _TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica); }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                System.Linq.Expressions.Expression<Func<BubbleSort.Dominio.Consultas, bool>> condicao = null;
                DateTime dataInicial = new DateTime(cmbDataInicial.Value.Year, cmbDataInicial.Value.Month, cmbDataInicial.Value.Day);
                DateTime dataFinal = new DateTime(cmbDataFinal.Value.Year, cmbDataFinal.Value.Month, cmbDataFinal.Value.Day, 23, 59, 59);
                if (((BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).Id == "" && ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id == "")
                    condicao = (x => x.Data >= dataInicial && x.Data <= dataFinal);
                else if (((BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).Id != "" && ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id == "")
                    condicao = (x => x.Paciente.Id == ((BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).Id && x.Data >= dataInicial && x.Data <= dataFinal);
                else if (((BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).Id == "" && ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id != "")
                    condicao = (x => x.Clinica.Id == ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id && x.Data >= dataInicial && x.Data <= dataFinal);
                else if (((BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).Id != "" && ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id != "")
                    condicao = (x => x.Paciente.Id == ((BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).Id && x.Clinica.Id == ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id && x.Data >= dataInicial && x.Data <= dataFinal);
                var lista = condicao == null ? _Repositorio.Todos() : _Repositorio.BuscarPor(condicao);

                reportDetalhesConsulta.LocalReport.EnableExternalImages = true;
                ReportParameterCollection parameters = new ReportParameterCollection();
                string LogoPath = "file://" + System.IO.Path.Combine(Application.StartupPath, @"Img\Logo.jpg");
                parameters.Add(new ReportParameter("parImgLogo", LogoPath));
                reportDetalhesConsulta.LocalReport.SetParameters(parameters);

                this.ConsultasBindingSource.DataSource = lista;
                this.reportDetalhesConsulta.RefreshReport();
            }
            catch (Exception ex)
            {
                _TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }
    }
}
