﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace ControlePacientes.Relatorio
{
    public partial class RelatorioPacientes : Form
    {
        public RelatorioPacientes(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Repositorio = new BubbleSort.Repositorio.PacienteRepositorio(session);
            _RepositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(session);
            _RepositorioConvenio = new BubbleSort.Repositorio.ConvenioRepositorio(session);
        }

        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private BubbleSort.Repositorio.PacienteRepositorio _Repositorio;
        private BubbleSort.Repositorio.ClinicaRepositorio _RepositorioClinica;
        private BubbleSort.Repositorio.ConvenioRepositorio _RepositorioConvenio;

        private void RelatorioCliente_Load(object sender, EventArgs e)
        {
            this.CarregarCombos();
        }

        private void CarregarCombos()
        {
            var listaClinicas = (List<BubbleSort.Dominio.Clinicas>)_RepositorioClinica.Todos();
            listaClinicas.Insert(0, new BubbleSort.Dominio.Clinicas() { Id = "", Nome = "Todas" });
            this.cmbClinica.DataSource = listaClinicas;

            var listaConvenio = (List<BubbleSort.Dominio.Convenios>)_RepositorioConvenio.Todos();
            listaConvenio.Insert(0, new BubbleSort.Dominio.Convenios() { Id = "", nomeConvenio = "Todos" });
            this.cmbConvenio.DataSource = listaConvenio;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                System.Linq.Expressions.Expression<Func<BubbleSort.Dominio.Paciente, bool>> condicao = null;
                if (((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id != "" && ((BubbleSort.Dominio.Convenios)cmbConvenio.SelectedItem).Id == "")
                    condicao = (x => x.Clinica.Id == ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id);
                else if (((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id != "" && ((BubbleSort.Dominio.Convenios)cmbConvenio.SelectedItem).Id != "")
                    condicao = (x => x.Clinica.Id == ((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id && x.Convenio.Id == ((BubbleSort.Dominio.Convenios)cmbConvenio.SelectedItem).Id);
                else if (((BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem).Id == "" && ((BubbleSort.Dominio.Convenios)cmbConvenio.SelectedItem).Id != "")
                    condicao = (x => x.Convenio.Id == ((BubbleSort.Dominio.Convenios)cmbConvenio.SelectedItem).Id);
                var lista = condicao == null ? _Repositorio.Todos() : _Repositorio.BuscarPor(condicao);

                reportPacientes.LocalReport.EnableExternalImages = true;
                ReportParameterCollection parameters = new ReportParameterCollection();
                string LogoPath = "file://" + System.IO.Path.Combine(Application.StartupPath, @"Img\Logo.jpg");
                parameters.Add(new ReportParameter("parImgLogo", LogoPath));
                reportPacientes.LocalReport.SetParameters(parameters);

                this.PacienteBindingSource.DataSource = lista;
                this.reportPacientes.RefreshReport();
            }
            catch (Exception ex)
            {
                _TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }
    }
}
