﻿namespace ControlePacientes.Controles
{
    partial class VisualizarConsulta
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtObservacaoConsulta = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtNome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtDataConsulta = new System.Windows.Forms.TextBox();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.txtClinica = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtProfissional = new System.Windows.Forms.TextBox();
            this.btnExames = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtObservacaoConsulta
            // 
            this.txtObservacaoConsulta.AcceptsTab = true;
            this.txtObservacaoConsulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObservacaoConsulta.Location = new System.Drawing.Point(101, 107);
            this.txtObservacaoConsulta.Name = "txtObservacaoConsulta";
            this.txtObservacaoConsulta.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtObservacaoConsulta.Size = new System.Drawing.Size(760, 195);
            this.txtObservacaoConsulta.TabIndex = 156;
            this.txtObservacaoConsulta.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 15);
            this.label4.TabIndex = 141;
            this.label4.Text = "Data Consulta:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 142;
            this.label1.Text = "Observação:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(580, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 143;
            this.label5.Text = "Clínica:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 15);
            this.label6.TabIndex = 144;
            this.label6.Text = "Profissional:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(430, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 15);
            this.label10.TabIndex = 147;
            this.label10.Text = "Telefone :";
            // 
            // TxtNome
            // 
            this.TxtNome.BackColor = System.Drawing.Color.White;
            this.TxtNome.Location = new System.Drawing.Point(101, 17);
            this.TxtNome.Name = "TxtNome";
            this.TxtNome.ReadOnly = true;
            this.TxtNome.Size = new System.Drawing.Size(314, 20);
            this.TxtNome.TabIndex = 139;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(49, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 15);
            this.label3.TabIndex = 140;
            this.label3.Text = "Nome :";
            // 
            // TxtTelefone
            // 
            this.TxtTelefone.Location = new System.Drawing.Point(497, 16);
            this.TxtTelefone.Mask = "(00)0000-0000";
            this.TxtTelefone.Name = "TxtTelefone";
            this.TxtTelefone.ReadOnly = true;
            this.TxtTelefone.Size = new System.Drawing.Size(131, 20);
            this.TxtTelefone.TabIndex = 145;
            // 
            // txtDataConsulta
            // 
            this.txtDataConsulta.BackColor = System.Drawing.Color.White;
            this.txtDataConsulta.Location = new System.Drawing.Point(101, 46);
            this.txtDataConsulta.Name = "txtDataConsulta";
            this.txtDataConsulta.ReadOnly = true;
            this.txtDataConsulta.Size = new System.Drawing.Size(198, 20);
            this.txtDataConsulta.TabIndex = 157;
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.Color.White;
            this.txtStatus.Location = new System.Drawing.Point(361, 45);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(198, 20);
            this.txtStatus.TabIndex = 158;
            // 
            // txtClinica
            // 
            this.txtClinica.BackColor = System.Drawing.Color.White;
            this.txtClinica.Location = new System.Drawing.Point(634, 43);
            this.txtClinica.Name = "txtClinica";
            this.txtClinica.ReadOnly = true;
            this.txtClinica.Size = new System.Drawing.Size(229, 20);
            this.txtClinica.TabIndex = 159;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(308, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 15);
            this.label11.TabIndex = 154;
            this.label11.Text = "Status :";
            // 
            // txtProfissional
            // 
            this.txtProfissional.BackColor = System.Drawing.Color.White;
            this.txtProfissional.Location = new System.Drawing.Point(101, 76);
            this.txtProfissional.Name = "txtProfissional";
            this.txtProfissional.ReadOnly = true;
            this.txtProfissional.Size = new System.Drawing.Size(198, 20);
            this.txtProfissional.TabIndex = 160;
            // 
            // btnExames
            // 
            this.btnExames.Location = new System.Drawing.Point(361, 73);
            this.btnExames.Name = "btnExames";
            this.btnExames.Size = new System.Drawing.Size(75, 23);
            this.btnExames.TabIndex = 161;
            this.btnExames.Text = "Exames";
            this.btnExames.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnExames);
            this.groupBox1.Controls.Add(this.TxtTelefone);
            this.groupBox1.Controls.Add(this.txtProfissional);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtClinica);
            this.groupBox1.Controls.Add(this.TxtNome);
            this.groupBox1.Controls.Add(this.txtStatus);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtDataConsulta);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtObservacaoConsulta);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(867, 308);
            this.groupBox1.TabIndex = 162;
            this.groupBox1.TabStop = false;
            // 
            // VisualizarConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "VisualizarConsulta";
            this.Size = new System.Drawing.Size(880, 322);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtObservacaoConsulta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtNome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox TxtTelefone;
        private System.Windows.Forms.TextBox txtDataConsulta;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.TextBox txtClinica;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtProfissional;
        private System.Windows.Forms.Button btnExames;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
