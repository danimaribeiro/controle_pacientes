﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes.Controles
{
    public partial class VisualizarConsulta : UserControl
    {
        private BubbleSort.Dominio.Consultas _Consulta;

        public VisualizarConsulta(BubbleSort.Dominio.Consultas consulta)
        {
            InitializeComponent();
            _Consulta = consulta;
        }

        private void SetarCampos()
        {
            TxtNome.Text = _Consulta.Paciente.Nome;
            TxtTelefone.Text = _Consulta.Paciente.TelefoneCelular;
            txtDataConsulta.Text = string.Format("{0:dd/MM/yyyy hh:mm}", _Consulta.Data);
            txtStatus.Text = _Consulta.status.ToString();
            txtClinica.Text = _Consulta.Clinica.Nome;
            txtProfissional.Text = _Consulta.Profissional.Nome;
            txtObservacaoConsulta.Rtf = _Consulta.Observacoes;
        }
    }
}
