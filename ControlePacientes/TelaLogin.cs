﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes
{
    public partial class TelaLogin : Form
    {
        public TelaLogin()
        {
            InitializeComponent();
        }

        private void TelaLogin_Load(object sender, EventArgs e)
        {
            System.Threading.Thread thread = new System.Threading.Thread(ThreadIniciaSessaoAssincrona);
            thread.Start();
        }

        private void ThreadIniciaSessaoAssincrona()
        {
            try
            {
                var t = BubbleSort.Repositorio.SessionHelper.FabricaSessao;
            } catch (Exception ex)
            {
                new BubbleSort.Servicos.TratadorDeErro().TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.Repositorio);
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            try
            {
                var repositorio = new BubbleSort.Repositorio.RepositorioUsuario(BubbleSort.Repositorio.SessionHelper.NovaSessao);
                var usuario = repositorio.BuscarRegistro(x => x.Username == txtUsuario.Text && x.Senha == txtSenha.Text);
                if (usuario == null)
                {
                    MessageBox.Show("Usuário ou senha inválidos", "Atencao!", MessageBoxButtons.OK, MessageBoxIcon.Information  );
                }
                else
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ex)
            {
                new BubbleSort.Servicos.TratadorDeErro().TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtSenha_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnEntrar.PerformClick();
        }
    }
}
