﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BubbleSort.Dominio;

namespace ControlePacientes
{
    public partial class CadastroPaciente : Form
    {
        public CadastroPaciente(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session, BubbleSort.Dominio.Paciente paciente = null)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Repositorio = new BubbleSort.Repositorio.PacienteRepositorio(session);
            _RepositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(session);
            _RepositorioConvenio = new BubbleSort.Repositorio.ConvenioRepositorio(session);
            _RepositorioProfissional = new BubbleSort.Repositorio.ProfissionalRepositorio(session);
            if (paciente == null)
                _VisualizandoCadastro = false;
            else
            {
                _VisualizandoCadastro = true;
                _Paciente = paciente;
            }
        }

        private Paciente _Paciente;
        private bool _VisualizandoCadastro;
        BindingSource _Binding;
        private List<Paciente> _ListaPaciente;
        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private BubbleSort.Repositorio.PacienteRepositorio _Repositorio;
        private BubbleSort.Repositorio.ClinicaRepositorio _RepositorioClinica;
        private BubbleSort.Repositorio.ProfissionalRepositorio _RepositorioProfissional;
        private BubbleSort.Repositorio.ConvenioRepositorio _RepositorioConvenio;

        private void CadastroPaciente_Load(object sender, EventArgs e)
        {
            try
            {
                CarregarCombos();
                if (!_VisualizandoCadastro)
                    this.BuscarTodosOsPacientes();
                this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
            } catch (Exception ex) { _TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica); }
        }

        private void CarregarCombos()
        {
            List<BubbleSort.Dominio.Clinicas> lista = (List<BubbleSort.Dominio.Clinicas>)_RepositorioClinica.Todos();
            lista.Insert(0, new BubbleSort.Dominio.Clinicas());
            this.cmbClinica.DataSource = lista;
            this.cmbClinica.SelectedIndex = 0;

            List<BubbleSort.Dominio.Profissionais> listaPro = (List<BubbleSort.Dominio.Profissionais>)_RepositorioProfissional.Todos();
            listaPro.Insert(0, new BubbleSort.Dominio.Profissionais());
            this.cmbProfissional.DataSource = listaPro;
            this.cmbProfissional.SelectedIndex = 0;

            List<BubbleSort.Dominio.Convenios> listaConve = (List<BubbleSort.Dominio.Convenios>)_RepositorioConvenio.Todos();
            listaConve.Insert(0, new BubbleSort.Dominio.Convenios());
            this.cmbConvenio.DataSource = listaConve;
            this.cmbConvenio.SelectedIndex = 0;
        }

        private void ValidaDados()
        {
            if (TxtNome.Text.Trim() == String.Empty)
                throw new Exception("O campo Nome é obrigatório.");
            if (TxtCPFCNPJ.Text.Trim() == String.Empty)
                throw new Exception("O campo CPF é obrigatório.");
            if (TxtTelefone.Text.Trim() == String.Empty)
                throw new Exception("O campo Telefone é obrigatório.");
            _Paciente.Nome = TxtNome.Text;
            _Paciente.Email = TxtEmail.Text;
            _Paciente.Cpf = TxtCPFCNPJ.Text;
            _Paciente.Rg = TxtRG_InscricaoEstadual.Text;
            _Paciente.TelefoneFixo = TxtTelefone.Text;
            _Paciente.TelefoneCelular = TxtTelefoneCelular.Text;
            _Paciente.Observacao = txtObservacao.Text;
            _Paciente.Endereco.Rua = TxtEndereço.Text;
            _Paciente.Endereco.Bairro = TxtBairro.Text;
            _Paciente.Endereco.Cep = TxtCep.Text;
            _Paciente.Endereco.Cidade = TxtCidade.Text;
            _Paciente.Endereco.Estado = TxtEstado.Text;
            _Paciente.Endereco.Numero = TxtEnderecoNumero.Text;
            _Paciente.Clinica = cmbClinica.SelectedIndex > 0 ? (BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem : null;
            _Paciente.Convenio = cmbConvenio.SelectedIndex > 0 ? (BubbleSort.Dominio.Convenios)cmbConvenio.SelectedItem : null;
            _Paciente.Profissional = cmbProfissional.SelectedIndex > 0 ? (BubbleSort.Dominio.Profissionais)cmbProfissional.SelectedItem : null;
        }

        private void EnabledCampos(bool enabled)
        {
            foreach (Control ctr in this.Controls)
            {
                if (ctr is ComboBox)
                    ((ComboBox)ctr).Enabled = enabled;
                else if (ctr is TextBox)
                    ((TextBox)ctr).Enabled = enabled;
                else if (ctr is MaskedTextBox)
                    ((MaskedTextBox)ctr).Enabled = enabled;
                else if (ctr is Button)
                    ((Button)ctr).Enabled = enabled;
                else if (ctr is DateTimePicker)
                    ((DateTimePicker)ctr).Enabled = enabled;
            }
        }

        private void CamposInterface(Controles.CtrNavigator.Status status)
        {
            if (_Paciente == null)
                _Paciente = new Paciente();
            txtCodigo.Text = _Paciente.Codigo.ToString();
            TxtNome.Text = _Paciente.Nome;
            TxtEmail.Text = _Paciente.Email;
            TxtCPFCNPJ.Text = _Paciente.Cpf;
            TxtRG_InscricaoEstadual.Text = _Paciente.Rg;
            TxtTelefone.Text = _Paciente.TelefoneFixo;
            TxtTelefoneCelular.Text = _Paciente.TelefoneCelular;
            TxtEndereço.Text = _Paciente.Endereco.Rua;
            TxtEnderecoNumero.Text = _Paciente.Endereco.Numero;
            TxtCep.Text = _Paciente.Endereco.Cep;
            TxtBairro.Text = _Paciente.Endereco.Bairro;
            TxtCidade.Text = _Paciente.Endereco.Cidade;
            TxtEstado.Text = _Paciente.Endereco.Estado;
            txtObservacao.Text = _Paciente.Observacao;
            cmbClinica.SelectedItem = _Paciente.Clinica;
            cmbConvenio.SelectedItem = _Paciente.Convenio;
            cmbProfissional.SelectedItem = _Paciente.Profissional;
            if (status == Controles.CtrNavigator.Status.Inserindo)
            {
                txtCodigo.Text = _Repositorio.ProximoCodigo().ToString();
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Inserindo";
            }
            else if (status == Controles.CtrNavigator.Status.Editando)
            {
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Editando";
            }
            else if (status == Controles.CtrNavigator.Status.Excluindo)
            {
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Excluindo";
            }
            else
            {
                this.ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Consultando";
            }
        }

        private void BuscarTodosOsPacientes()
        {
            _ListaPaciente = (List<BubbleSort.Dominio.Paciente>)_Repositorio.Todos();
            if (_ListaPaciente == null)
                _ListaPaciente = new List<Paciente>();
            _Binding = new BindingSource(_ListaPaciente, "");
            _Binding.ResetBindings(true);
            this.DGPacientes.AutoGenerateColumns = false;
            this.DGPacientes.DataSource = _Binding;
            ctrNavigator1.DataSource = _Binding;
        }

        private void ctrNavigator1_MudaRegistroSelecionado(object objetoAtual)
        {
            DGPacientes.Rows[ctrNavigator1.Indice].Selected = true;
        }

        private void ctrNavigator1_EventoNovo()
        {
            _Paciente = null;
            _Paciente = new Paciente();
            this.CamposInterface(Controles.CtrNavigator.Status.Inserindo);
        }

        private void ctrNavigator1_EditarRegistro(object objEditar)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Editando);
        }

        private void ctrNavigator1_CancelarAcao()
        {
            if (DGPacientes.SelectedRows.Count > 0)
                _Paciente = (Paciente)DGPacientes.SelectedRows[0].DataBoundItem;
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void ctrNavigator1_SalvarRegistro(object objSalvar)
        {
            try
            {
                this.ValidaDados();
                if (string.IsNullOrWhiteSpace(_Paciente.Id))
                {
                    _Repositorio.Adicionar(_Paciente);
                    _ListaPaciente.Add(_Paciente);
                }
                else
                    _Repositorio.Atualizar(_Paciente);
                if (_VisualizandoCadastro)
                    this.Close();
                else
                {
                    _Binding.ResetBindings(true);
                    ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                    this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
                }
            } catch (Exception ex)
            {
                this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        private void ctrNavigator1_ExcluirRegistro(object objExcluir)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Excluindo);
            if (MessageBox.Show("Deseja excluir o registro.", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    _Repositorio.Deletar(_Paciente);
                    this._ListaPaciente.Remove(_Paciente);
                    this._Binding.ResetBindings(true);
                } catch (Exception ex)
                {
                    this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void DGClinicas_SelectionChanged(object sender, EventArgs e)
        {
            if (this.DGPacientes.Rows.Count > 0)
            {
                if (DGPacientes.SelectedRows.Count > 0)
                {
                    _Paciente = (Paciente)DGPacientes.SelectedRows[0].DataBoundItem;
                    if (ctrNavigator1.DataSource != null)
                        ctrNavigator1.Indice = DGPacientes.SelectedRows[0].Index;
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void txtObservacao_TextChanged(object sender, EventArgs e)
        {
            lblCaracteresRestantes.Text = (txtObservacao.MaxLength - txtObservacao.Text.Length) + " caracteres restantes.";
        }
    }
}
