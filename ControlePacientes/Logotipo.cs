﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes
{
    public partial class Logotipo : Form
    {
        public Logotipo()
        {
            InitializeComponent();
        }

        private string _Arquivo;
        private string _ArquivoSelecionado;

        private void Logotipo_Load(object sender, EventArgs e)
        {
            _Arquivo = System.IO.Path.Combine(Application.StartupPath, @"Img\logo.jpg");
            _ArquivoSelecionado = _Arquivo;
            if (System.IO.File.Exists(_Arquivo))
                this.CarregarLogo(_Arquivo);
        }

        private void btnSelecionarLogo_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Imagens |*.jpg;*.jpeg;*.png";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _ArquivoSelecionado = ofd.FileName;
                this.CarregarLogo(ofd.FileName);
            }
        }

        private void CarregarLogo(string logo)
        {
            try
            {
                System.IO.FileStream fs = new System.IO.FileStream(logo, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                Image img = Image.FromStream(fs);
                fs.Close();
                pcbLogo.Image = img;
            }
            catch (Exception ex) { }
        }

        private void btnSalvarLogo_Click(object sender, EventArgs e)
        {
            if (_ArquivoSelecionado != _Arquivo)
            {
                System.IO.File.SetAttributes(System.IO.Path.Combine(Application.StartupPath, @"Img\logo.jpg"), System.IO.FileAttributes.Normal);
                System.IO.File.Copy(_ArquivoSelecionado, System.IO.Path.Combine(Application.StartupPath, @"Img\logo.jpg"), true);
                _ArquivoSelecionado = _Arquivo;
                MessageBox.Show("Logo salva com sucesso.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }
    }
}
