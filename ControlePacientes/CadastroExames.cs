﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BubbleSort.Dominio;

namespace ControlePacientes
{
    public partial class CadastroExames : Form
    {
        public CadastroExames(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Repositorio = new BubbleSort.Repositorio.ExameRepositorio(session);
            _RepositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(session);
        }

        private Exames _Exame;
        BindingSource _Binding;
        private List<Exames> _ListaExames;
        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private BubbleSort.Repositorio.ExameRepositorio _Repositorio;
        private BubbleSort.Repositorio.ClinicaRepositorio _RepositorioClinica;

        private void BuscarTodasAsClinicas()
        {
            var lista = (List<BubbleSort.Dominio.Clinicas>)_RepositorioClinica.Todos();
            lista.Insert(0, new BubbleSort.Dominio.Clinicas());
            this.cmbClinica.DataSource = lista;
            this.cmbClinica.SelectedIndex = 0;
        }

        private void ValidaDados()
        {
            if (TxtNomeExame.Text.Trim() == String.Empty)
                throw new Exception("O campo Nome é obrigatório.");
            if (cmbClinica.SelectedItem == null)
                throw new Exception("A clínica é obrigatória.");
            _Exame.nomeExame = TxtNomeExame.Text;
            _Exame.observacao = txtObservacao.Text;
            _Exame.clinica = cmbClinica.SelectedIndex > 0 ? (Clinicas)cmbClinica.SelectedItem : null;
        }

        private void EnabledCampos(bool enabled)
        {
            foreach (Control ctr in this.Controls)
            {
                if (ctr is ComboBox)
                    ((ComboBox)ctr).Enabled = enabled;
                else if (ctr is TextBox)
                    ((TextBox)ctr).Enabled = enabled;
                else if (ctr is MaskedTextBox)
                    ((MaskedTextBox)ctr).Enabled = enabled;
                else if (ctr is Button)
                    ((Button)ctr).Enabled = enabled;
                else if (ctr is DateTimePicker)
                    ((DateTimePicker)ctr).Enabled = enabled;
            }
        }

        private void CamposInterface(Controles.CtrNavigator.Status status)
        {
            if (_Exame == null)
                _Exame = new Exames();
            txtCodigo.Text = _Exame.Codigo.ToString();
            TxtNomeExame.Text = _Exame.nomeExame;
            txtObservacao.Text = _Exame.observacao;
            cmbClinica.SelectedItem = _Exame.clinica;
            if (status == Controles.CtrNavigator.Status.Inserindo)
            {
                txtCodigo.Text = _Repositorio.ProximoCodigo().ToString();
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Inserindo";
            }
            else if (status == Controles.CtrNavigator.Status.Editando)
            {
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Editando";
            }
            else if (status == Controles.CtrNavigator.Status.Excluindo)
            {
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Excluindo";
            }
            else
            {
                this.ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Consultando";
            }
        }

        private void BuscarTodosOsExames()
        {
            _ListaExames = (List<BubbleSort.Dominio.Exames>)_Repositorio.Todos();
            _Binding = new BindingSource(_ListaExames, "");
            _Binding.ResetBindings(true);
            this.DGExames.AutoGenerateColumns = false;
            this.DGExames.DataSource = _Binding;
            ctrNavigator1.DataSource = _Binding;
        }

        private void CadastroExames_Load(object sender, EventArgs e)
        {
            try
            {
                this.BuscarTodasAsClinicas();
                this.BuscarTodosOsExames();
                this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
            } catch (Exception ex)
            {
                _TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        private void ctrNavigator1_MudaRegistroSelecionado(object objetoAtual)
        {
            DGExames.Rows[ctrNavigator1.Indice].Selected = true;
        }

        private void ctrNavigator1_EventoNovo()
        {
            _Exame = null;
            _Exame = new Exames();
            this.CamposInterface(Controles.CtrNavigator.Status.Inserindo);
        }

        private void ctrNavigator1_EditarRegistro(object objEditar)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Editando);
        }

        private void ctrNavigator1_CancelarAcao()
        {
            if (DGExames.SelectedRows.Count > 0)
                _Exame = (Exames)DGExames.SelectedRows[0].DataBoundItem;
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void ctrNavigator1_SalvarRegistro(object objSalvar)
        {
            try
            {
                this.ValidaDados();
                if (string.IsNullOrWhiteSpace(_Exame.Id))
                {
                    _Repositorio.Adicionar(_Exame);
                    _ListaExames.Add(_Exame);
                }
                else
                    _Repositorio.Atualizar(_Exame);
                _Binding.ResetBindings(true);
                ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
            } catch (Exception ex)
            {
                this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        private void ctrNavigator1_ExcluirRegistro(object objExcluir)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Excluindo);
            if (MessageBox.Show("Deseja excluir o registro.", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    _Repositorio.Deletar(_Exame);
                    this._ListaExames.Remove(_Exame);
                    this._Binding.ResetBindings(true);
                } catch (Exception ex)
                {
                    this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void DGProfissionais_SelectionChanged(object sender, EventArgs e)
        {
            if (this.DGExames.Rows.Count > 0)
            {
                if (DGExames.SelectedRows.Count > 0)
                {
                    _Exame = (Exames)DGExames.SelectedRows[0].DataBoundItem;
                    if (ctrNavigator1.DataSource != null)
                        ctrNavigator1.Indice = DGExames.SelectedRows[0].Index;
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void txtObservacao_TextChanged(object sender, EventArgs e)
        {
            lblCaracteresRestantes.Text = (txtObservacao.MaxLength - txtObservacao.Text.Length) + " caracteres restantes.";
        }
    }
}
