﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes
{
    public partial class ConsultasPaciente : Form
    {
        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private NHibernate.ISession _Session;

        private BubbleSort.Repositorio.ExameRepositorio _ExameRepositorio;
        private BubbleSort.Repositorio.ClinicaRepositorio _ClinicaRepositorio;
        private BubbleSort.Repositorio.PacienteRepositorio _PacienteRepositorio;
        private BubbleSort.Repositorio.ConsultaRepositorio _ConsultaRepositorio;

        public ConsultasPaciente(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Session = session;
            _ExameRepositorio = new BubbleSort.Repositorio.ExameRepositorio(session);
            _ClinicaRepositorio = new BubbleSort.Repositorio.ClinicaRepositorio(session);
            _PacienteRepositorio = new BubbleSort.Repositorio.PacienteRepositorio(session);
            _ConsultaRepositorio = new BubbleSort.Repositorio.ConsultaRepositorio(session);
        }


        private void ConsultasPaciente_Load(object sender, EventArgs e)
        {
            var clinicas = _ClinicaRepositorio.Todos(x => x.Id);
            cmbClinica.DataSource = clinicas;
            if (clinicas.Count > 0)
            {
                List<BubbleSort.Dominio.Paciente> pacientes = (List<BubbleSort.Dominio.Paciente>)_PacienteRepositorio.BuscarPor(x => x.Clinica.Id == clinicas[0].Id, x => x.Nome);
                pacientes.Insert(0, new BubbleSort.Dominio.Paciente() { Id = "", Nome = "Selecione" });
                cmbPaciente.DataSource = pacientes;
                //cmbPaciente.SelectedIndexChanged += new EventHandler(cmbPaciente_SelectedIndexChanged);
            }

            System.Threading.Thread thread = new System.Threading.Thread(CarregarConsultas);
            thread.Start();

        }

        void cmbPaciente_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbPaciente.SelectedItem != null)
            {
                var paciente = (BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem;
                if (paciente.Id != "")
                {
                    TxtNome.Text = paciente.Nome;
                    TxtEmail.Text = paciente.Email;
                    TxtTelefone.Text = paciente.TelefoneFixo;
                    txtCelular.Text = paciente.TelefoneCelular;
                }
            }
        }

        private BubbleSort.Dominio.Paciente _PacienteSelecionado;

        private void CarregarConsultas()
        {
            this.BeginInvoke((Action)(() =>
            {
                LblStatus.Text = "Carregando consultas";
            }));

            List<BubbleSort.Dominio.Consultas> proximasConsultas = null;
            if (_PacienteSelecionado != null)
                proximasConsultas = (List<BubbleSort.Dominio.Consultas>)_ConsultaRepositorio.BuscarPor(x => x.Data >= DateTime.Now.Date && x.Paciente.Id == _PacienteSelecionado.Id);
            this.BeginInvoke((Action)(() =>
            {
                gdvProximasConsultas.AutoGenerateColumns = false;
                gdvProximasConsultas.DataSource = proximasConsultas;
            }));

            List<BubbleSort.Dominio.Consultas> consultaAnteriores = null;
            if (_PacienteSelecionado != null)
                consultaAnteriores = (List<BubbleSort.Dominio.Consultas>)_ConsultaRepositorio.BuscarPor(x => x.Data <= DateTime.Now.Date && x.Paciente.Id == _PacienteSelecionado.Id, x => x.Data);
            this.BeginInvoke((Action)(() =>
            {
                gdvConsultasAnteriores.AutoGenerateColumns = false;
                gdvConsultasAnteriores.DataSource = consultaAnteriores;
            }));

            this.BeginInvoke((Action)(() =>
            {
                LblStatus.Text = "Finalizado";
            }));
        }

        private void atualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread thread = new System.Threading.Thread(CarregarConsultas);
            thread.Start();
        }

        private void cmbClinica_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbClinica.SelectedItem != null)
            {
                var clinica = (BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem;
                if (clinica != null)
                {
                    List<BubbleSort.Dominio.Paciente> pacientes = (List<BubbleSort.Dominio.Paciente>)_PacienteRepositorio.BuscarPor(x => x.Clinica.Id == clinica.Id, x => x.Nome);
                    pacientes.Insert(0, new BubbleSort.Dominio.Paciente() { Id = "", Nome = "Selecione" });
                    cmbPaciente.DataSource = pacientes;
                }
            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            if (cmbPaciente.SelectedIndex >= 0)
                _PacienteSelecionado = (BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem;
            else
                _PacienteSelecionado = null;

            System.Threading.Thread thread = new System.Threading.Thread(CarregarConsultas);
            thread.Start();
        }

        private void btnNovaConsulta_Click(object sender, EventArgs e)
        {
            if (cmbPaciente.SelectedItem != null)
            {
                if (((BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).Id != "")
                {
                    new NovaConsulta(_TratadorDeErro, _Session, (BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).ShowDialog();
                }
                else
                    MessageBox.Show("Por favor selecion o Paciente primeiro!", "Atenção!");
            }
            else
                MessageBox.Show("Por favor selecion o Paciente primeiro!", "Atenção!");
        }

        private void gdvProximasConsultas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 3)
                {
                    var consulta = (BubbleSort.Dominio.Consultas)gdvProximasConsultas.Rows[e.RowIndex].DataBoundItem;
                    new NovaConsulta(_TratadorDeErro, _Session, consulta.Paciente, consulta).ShowDialog();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (cmbPaciente.SelectedItem != null)
            {
                var paciente = (BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem;
                new CadastroPaciente(_TratadorDeErro, _Session, paciente).ShowDialog();
            }
        }

        private void gdvConsultasAnteriores_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 3)
                {
                    var consulta = (BubbleSort.Dominio.Consultas)gdvConsultasAnteriores.Rows[e.RowIndex].DataBoundItem;
                    new NovaConsulta(_TratadorDeErro, _Session, consulta.Paciente, consulta).ShowDialog();
                }
            }
        }

        private void btnHistorico_Click(object sender, EventArgs e)
        {
            if (cmbPaciente.SelectedItem != null)
            {
                if (((BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem).Id != "")
                {
                    var paciente = (BubbleSort.Dominio.Paciente)cmbPaciente.SelectedItem;
                    new HistoricoPaciente(_TratadorDeErro, _Session, paciente).ShowDialog();
                }
                else
                    MessageBox.Show("Por favor selecion o Paciente primeiro!", "Atenção!");
            }
            else
                MessageBox.Show("Por favor selecion o Paciente primeiro!", "Atenção!");
        }
    }
}
