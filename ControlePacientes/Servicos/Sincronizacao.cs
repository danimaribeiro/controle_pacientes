﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using Npgsql;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.Server;
using Microsoft.Synchronization.Data.SqlServerCe;
using ControlePacientes.Servicos.Synchronization.Data.PostgreSQL;
using Microsoft.Synchronization;
using System.Data.SqlServerCe;
using System.Data;
using NpgsqlTypes;

namespace ControlePacientes.Servicos
{
    public class Sincronizacao
    {
        public event EventHandler<SyncProgressEventArgs> SyncProgress;
        public event EventHandler<ApplyChangeFailedEventArgs> ClientApplyChangeFailed;
        public event EventHandler<ApplyChangeFailedEventArgs> ServerApplyChangeFailed;

        NpgsqlConnection serverConnection = null;
        private BubbleSort.Dominio.Repositorio.IRepositorioClinica _ClinicaRepositorio;
        private BubbleSort.Dominio.Repositorio.IRepositorioConsulta _ConsultaRepositorio;
        private BubbleSort.Dominio.Repositorio.IRepositorioConvenio _ConvenioRepositorio;
        private BubbleSort.Dominio.Repositorio.IRepositorioEndereco _EnderecoRepositorio;
        private BubbleSort.Dominio.Repositorio.IRepositorioExame _ExameRepositorio;
        private BubbleSort.Dominio.Repositorio.IRepositorioPaciente _PacienteRepositorio;
        private BubbleSort.Dominio.Repositorio.IRepositorioProfissional _ProfissionalRepositorio;
        private BubbleSort.Dominio.Repositorio.IRepositorioUsuario _UsuarioRepositorio;
        
        public void SynchronizeUp()
        {
            serverConnection = new NpgsqlConnection("Server=127.0.0.1;Port=5432;Database=pacientes;User ID=postgres;Password=ti73dejk;Pooling=true;");

            var repositorioRegistros = new BubbleSort.Repositorio.RepositorioRegistrosAtualizar(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var repositorio = new BubbleSort.Repositorio.UltimaAtualizacaoRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var atualizacao = repositorio.BuscarRegistro(x => x.Codigo == 1L);
            DateTime ultimaAtualizacao = DateTime.MinValue;
            if (atualizacao != null)
                ultimaAtualizacao = atualizacao.DataAtualizacao;
            else
            {
                atualizacao = new BubbleSort.Dominio.Sincronizacao.UltimaAtualizacao();
                atualizacao.DataAtualizacao = DateTime.Now;
                atualizacao.Codigo = 1;
                atualizacao.Id = "1";
                repositorio.Adicionar(atualizacao);
            }
            atualizacao.DataAtualizacao = DateTime.Now;
            var registros = repositorioRegistros.BuscarPor(x => x.DataAtualizacao > ultimaAtualizacao);
            foreach (var item in registros)
            {
                var tabela = item.Tabela;
                switch (tabela)
                {
                    case "tb_clinica":
                        SincronizaClinica(item);
                        break;
                    case "tb_consultas":
                        SincronizaConsultas(item);
                        break;
                    case "tb_convenio":
                        SincronizaConvenio(item);
                        break;
                    case "tb_exame":
                        SincronizaExame(item);
                        break;
                    case "tb_paciente":
                        SincronizaPaciente(item);
                        break;
                    case "tb_profissional":
                        SincronizaProfissional(item);
                        break;
                    case "tb_usuario":
                        SincronizaUsuario(item);
                        break;
                }
            }
            repositorio.Atualizar(atualizacao);
        }

        //OK
        private void SincronizaClinica(BubbleSort.Dominio.Sincronizacao.RegistrosAtualizar registro)
        {
            var repositorioLocal = new BubbleSort.Repositorio.ClinicaRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var repositorio = new BubbleSort.Repositorio.ClinicaRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var clinica = repositorioLocal.BuscarPorId(registro.uuid);
            var novoRegistro = new BubbleSort.Dominio.Clinicas();
            novoRegistro.Endereco = new BubbleSort.Dominio.Endereco();

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update || registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Delete)
                novoRegistro = repositorio.BuscarPorId(registro.uuid);

            novoRegistro.Cnpj = clinica.Cnpj;
            novoRegistro.Codigo = clinica.Codigo;
            novoRegistro.Endereco.Id = clinica.Endereco.Id;
            novoRegistro.Endereco.Bairro = clinica.Endereco.Bairro;
            novoRegistro.Endereco.Cep = clinica.Endereco.Cep;
            novoRegistro.Endereco.Cidade = clinica.Endereco.Cidade;
            novoRegistro.Endereco.Codigo = clinica.Endereco.Codigo;
            novoRegistro.Endereco.Estado = clinica.Endereco.Estado;
            novoRegistro.Endereco.Numero = clinica.Endereco.Numero;
            novoRegistro.Endereco.Rua = clinica.Endereco.Rua;
            novoRegistro.Id = clinica.Id;
            novoRegistro.InscricaoEstadual = clinica.InscricaoEstadual;
            novoRegistro.Nome = clinica.Nome;
            novoRegistro.NomeFantasia = clinica.NomeFantasia;
            novoRegistro.TelefoneClinica = clinica.TelefoneClinica;
            novoRegistro.TelefoneFax = clinica.TelefoneFax;

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Insert)
                repositorio.Adicionar(novoRegistro);
            else if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update)
                repositorio.Atualizar(novoRegistro);
            else
                repositorio.Deletar(novoRegistro);
        }
                
        private void SincronizaConsultas(BubbleSort.Dominio.Sincronizacao.RegistrosAtualizar registro)
        {
            var repositorioLocal = new BubbleSort.Repositorio.ConsultaRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var repositorio = new BubbleSort.Repositorio.ConsultaRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var consulta = repositorioLocal.BuscarPorId(registro.uuid);
            var novoRegistro = new BubbleSort.Dominio.Consultas();

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update || registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Delete)
                novoRegistro = repositorio.BuscarPorId(registro.uuid);

            if (consulta.Clinica != null)
                novoRegistro.Clinica = new BubbleSort.Dominio.Clinicas() { Id = consulta.Clinica.Id };
            novoRegistro.Codigo = consulta.Codigo;
            novoRegistro.Data = consulta.Data;
            novoRegistro.Id = consulta.Id;
            novoRegistro.Observacoes = consulta.Observacoes;
            if (consulta.Paciente != null)
                novoRegistro.Paciente = new BubbleSort.Dominio.Paciente() { Id = consulta.Paciente.Id };
            if (consulta.Profissional != null)
                novoRegistro.Profissional = new BubbleSort.Dominio.Profissionais() { Id = consulta.Profissional.Id };
            novoRegistro.status = consulta.status;
            novoRegistro.Tipo = consulta.Tipo;

            if (consulta.ListaExames != null)
            {
                foreach (var item in consulta.ListaExames)
                {
                    //TODO Passar a lista de exames também.
                }
            }

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Insert)
                repositorio.Adicionar(novoRegistro);
            else if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update)
                repositorio.Atualizar(novoRegistro);
            else
                repositorio.Deletar(novoRegistro);

        }

        //OK
        private void SincronizaConvenio(BubbleSort.Dominio.Sincronizacao.RegistrosAtualizar registro)
        {
            var repositorioLocal = new BubbleSort.Repositorio.ConvenioRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var repositorio = new BubbleSort.Repositorio.ConvenioRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var repositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var convenio = repositorioLocal.BuscarPorId(registro.uuid);
            var novoRegistro = new BubbleSort.Dominio.Convenios();

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update || registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Delete)
                novoRegistro = repositorio.BuscarPorId(registro.uuid);

            if (convenio != null && novoRegistro.clinica == null)
                novoRegistro.clinica = new BubbleSort.Dominio.Clinicas() { Id = convenio.clinica.Id, Endereco = new BubbleSort.Dominio.Endereco() { Id = convenio.clinica.Endereco.Id } };
            else if(convenio!=null)            
                novoRegistro.clinica = repositorioClinica.BuscarPorId(convenio.clinica.Id);
            
            novoRegistro.Codigo = convenio.Codigo;
            novoRegistro.Id = convenio.Id;
            novoRegistro.nomeConvenio = convenio.nomeConvenio;
            novoRegistro.observacao = convenio.observacao;

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Insert)
                repositorio.Adicionar(novoRegistro);
            else if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update)
                repositorio.Atualizar(novoRegistro);
            else
                repositorio.Deletar(novoRegistro);
        }

        //OK
        private void SincronizaExame(BubbleSort.Dominio.Sincronizacao.RegistrosAtualizar registro)
        {
            var repositorioLocal = new BubbleSort.Repositorio.ExameRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var repositorio = new BubbleSort.Repositorio.ExameRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var repositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var exame = repositorioLocal.BuscarPorId(registro.uuid);
            var novoRegistro = new BubbleSort.Dominio.Exames();

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update || registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Delete)
                novoRegistro = repositorio.BuscarPorId(registro.uuid);

            if (exame != null && novoRegistro.clinica == null)
                novoRegistro.clinica = new BubbleSort.Dominio.Clinicas() { Id = exame.clinica.Id, Endereco = new BubbleSort.Dominio.Endereco() { Id = exame.clinica.Endereco.Id } };
            else if (exame != null)
                novoRegistro.clinica = repositorioClinica.BuscarPorId(exame.clinica.Id);

            novoRegistro.Codigo = exame.Codigo;
            novoRegistro.Id = exame.Id;
            novoRegistro.nomeExame = exame.nomeExame;
            novoRegistro.observacao = exame.observacao;

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Insert)
                repositorio.Adicionar(novoRegistro);
            else if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update)
                repositorio.Atualizar(novoRegistro);
            else
                repositorio.Deletar(novoRegistro);
        }

        private void SincronizaPaciente(BubbleSort.Dominio.Sincronizacao.RegistrosAtualizar registro)
        {
            var repositorioLocal = new BubbleSort.Repositorio.PacienteRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var repositorio = new BubbleSort.Repositorio.PacienteRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var paciente = repositorioLocal.BuscarPorId(registro.uuid);
            var novoRegistro = new BubbleSort.Dominio.Paciente();
            novoRegistro.Endereco = new BubbleSort.Dominio.Endereco();

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update || registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Delete)
                novoRegistro = repositorio.BuscarPorId(registro.uuid);

            if (paciente.Clinica != null)
                novoRegistro.Clinica = new BubbleSort.Dominio.Clinicas() { Id = paciente.Clinica.Id };
            novoRegistro.Codigo = paciente.Codigo;
            if (paciente.Convenio != null)
                novoRegistro.Convenio = new BubbleSort.Dominio.Convenios() { Id = paciente.Convenio.Id };
            novoRegistro.Cpf = paciente.Cpf;
            novoRegistro.Email = paciente.Email;
            novoRegistro.Endereco.Id = paciente.Endereco.Id;
            novoRegistro.Endereco.Bairro = paciente.Endereco.Bairro;
            novoRegistro.Endereco.Cep = paciente.Endereco.Cep;
            novoRegistro.Endereco.Cidade = paciente.Endereco.Cidade;
            novoRegistro.Endereco.Codigo = paciente.Endereco.Codigo;
            novoRegistro.Endereco.Estado = paciente.Endereco.Estado;
            novoRegistro.Endereco.Numero = paciente.Endereco.Numero;
            novoRegistro.Endereco.Rua = paciente.Endereco.Rua;
            novoRegistro.Id = paciente.Id;
            novoRegistro.Nome = paciente.Nome;
            novoRegistro.Observacao = paciente.Observacao;
            if (paciente.Profissional != null)
                novoRegistro.Profissional = new BubbleSort.Dominio.Profissionais() { Id = paciente.Profissional.Id };
            novoRegistro.Rg = paciente.Rg;
            novoRegistro.TelefoneCelular = paciente.TelefoneCelular;
            novoRegistro.TelefoneFixo = paciente.TelefoneFixo;

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Insert)
                repositorio.Adicionar(novoRegistro);
            else if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update)
                repositorio.Atualizar(novoRegistro);
            else
                repositorio.Deletar(novoRegistro);
        }

        private void SincronizaProfissional(BubbleSort.Dominio.Sincronizacao.RegistrosAtualizar registro)
        {
            var repositorioLocal = new BubbleSort.Repositorio.ProfissionalRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var repositorio = new BubbleSort.Repositorio.ProfissionalRepositorio(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var profissional = repositorioLocal.BuscarPorId(registro.uuid);
            var novoRegistro = new BubbleSort.Dominio.Profissionais();
            novoRegistro.Endereco = new BubbleSort.Dominio.Endereco();

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update || registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Delete)
                novoRegistro = repositorio.BuscarPorId(registro.uuid);

            if (profissional.Clinica != null)
                novoRegistro.Clinica = new BubbleSort.Dominio.Clinicas() { Id = profissional.Clinica.Id };
            novoRegistro.Codigo = profissional.Codigo;
            novoRegistro.Cpf = profissional.Cpf;
            novoRegistro.DataNascimento = profissional.DataNascimento;
            novoRegistro.Email = profissional.Email;
            novoRegistro.Endereco.Id = profissional.Endereco.Id;
            novoRegistro.Endereco.Bairro = profissional.Endereco.Bairro;
            novoRegistro.Endereco.Cep = profissional.Endereco.Cep;
            novoRegistro.Endereco.Cidade = profissional.Endereco.Cidade;
            novoRegistro.Endereco.Codigo = profissional.Endereco.Codigo;
            novoRegistro.Endereco.Estado = profissional.Endereco.Estado;
            novoRegistro.Endereco.Numero = profissional.Endereco.Numero;
            novoRegistro.Endereco.Rua = profissional.Endereco.Rua;
            novoRegistro.Id = profissional.Id;
            novoRegistro.Nome = profissional.Nome;
            novoRegistro.Observacao = profissional.Observacao;
            novoRegistro.Rg = profissional.Rg;
            novoRegistro.TelefoneCelular = profissional.TelefoneCelular;
            novoRegistro.TelefoneFixo = profissional.TelefoneFixo;

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Insert)
                repositorio.Adicionar(novoRegistro);
            else if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update)
                repositorio.Atualizar(novoRegistro);
            else
                repositorio.Deletar(novoRegistro);
        }

        //OK
        private void SincronizaUsuario(BubbleSort.Dominio.Sincronizacao.RegistrosAtualizar registro)
        {
            var repositorioLocal = new BubbleSort.Repositorio.RepositorioUsuario(BubbleSort.Repositorio.SessionHelper.NovaSessao);
            var repositorio = new BubbleSort.Repositorio.RepositorioUsuario(BubbleSort.Repositorio.SessionHelper.NovaSessaoServer);
            var usuario = repositorioLocal.BuscarPorId(registro.uuid);
            var novoRegistro = new BubbleSort.Dominio.Usuario();

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update || registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Delete)
                novoRegistro = repositorio.BuscarPorId(registro.uuid);

            novoRegistro.Codigo = usuario.Codigo;
            novoRegistro.Id = usuario.Id;
            novoRegistro.Nome = usuario.Nome;
            novoRegistro.Senha = usuario.Senha;
            novoRegistro.Username = usuario.Username;

            if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Insert)
                repositorio.Adicionar(novoRegistro);
            else if (registro.TipoAtualizacao == BubbleSort.Dominio.Enumeradores.TipoUpdate.Update)
                repositorio.Atualizar(novoRegistro);
            else
                repositorio.Deletar(novoRegistro);
        }
    }
}

