﻿namespace ControlePacientes
{
    partial class CadastroExames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroExames));
            this.label13 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtNomeExame = new System.Windows.Forms.TextBox();
            this.cmbClinica = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.DGExames = new System.Windows.Forms.DataGridView();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.LblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ctrNavigator1 = new ControlePacientes.Controles.CtrNavigator();
            this.lblCaracteresRestantes = new System.Windows.Forms.Label();
            this.ColCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNomeFantasia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DGExames)).BeginInit();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(66, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 15);
            this.label13.TabIndex = 57;
            this.label13.Text = "Código :";
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.White;
            this.txtCodigo.Enabled = false;
            this.txtCodigo.Location = new System.Drawing.Point(124, 8);
            this.txtCodigo.MaxLength = 2;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Size = new System.Drawing.Size(131, 20);
            this.txtCodigo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 15);
            this.label1.TabIndex = 55;
            this.label1.Text = "Nome do Exame :";
            // 
            // TxtNomeExame
            // 
            this.TxtNomeExame.BackColor = System.Drawing.Color.White;
            this.TxtNomeExame.Enabled = false;
            this.TxtNomeExame.Location = new System.Drawing.Point(124, 36);
            this.TxtNomeExame.Name = "TxtNomeExame";
            this.TxtNomeExame.Size = new System.Drawing.Size(414, 20);
            this.TxtNomeExame.TabIndex = 1;
            // 
            // cmbClinica
            // 
            this.cmbClinica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClinica.FormattingEnabled = true;
            this.cmbClinica.Location = new System.Drawing.Point(124, 63);
            this.cmbClinica.Name = "cmbClinica";
            this.cmbClinica.Size = new System.Drawing.Size(414, 21);
            this.cmbClinica.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(67, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 15);
            this.label9.TabIndex = 122;
            this.label9.Text = "Clinica :";
            // 
            // txtObservacao
            // 
            this.txtObservacao.BackColor = System.Drawing.Color.White;
            this.txtObservacao.Enabled = false;
            this.txtObservacao.Location = new System.Drawing.Point(124, 92);
            this.txtObservacao.MaxLength = 255;
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtObservacao.Size = new System.Drawing.Size(414, 68);
            this.txtObservacao.TabIndex = 3;
            this.txtObservacao.TextChanged += new System.EventHandler(this.txtObservacao_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(36, 94);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 15);
            this.label14.TabIndex = 124;
            this.label14.Text = "Observação :";
            // 
            // DGExames
            // 
            this.DGExames.AllowUserToAddRows = false;
            this.DGExames.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DGExames.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGExames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGExames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGExames.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCodigo,
            this.ColNome,
            this.ColCNPJ,
            this.ColNomeFantasia});
            this.DGExames.Location = new System.Drawing.Point(17, 224);
            this.DGExames.MultiSelect = false;
            this.DGExames.Name = "DGExames";
            this.DGExames.ReadOnly = true;
            this.DGExames.RowHeadersWidth = 15;
            this.DGExames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGExames.Size = new System.Drawing.Size(679, 268);
            this.DGExames.TabIndex = 5;
            this.DGExames.SelectionChanged += new System.EventHandler(this.DGProfissionais_SelectionChanged);
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblStatus});
            this.StatusStrip.Location = new System.Drawing.Point(0, 510);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(707, 22);
            this.StatusStrip.TabIndex = 126;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // LblStatus
            // 
            this.LblStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(124, 17);
            this.LblStatus.Text = "Status : Consultando";
            // 
            // ctrNavigator1
            // 
            this.ctrNavigator1.DataSource = null;
            this.ctrNavigator1.Indice = -1;
            this.ctrNavigator1.Location = new System.Drawing.Point(17, 176);
            this.ctrNavigator1.Name = "ctrNavigator1";
            this.ctrNavigator1.Size = new System.Drawing.Size(547, 42);
            this.ctrNavigator1.TabIndex = 4;
            this.ctrNavigator1.MudaRegistroSelecionado += new ControlePacientes.Controles.CtrNavigator.MudaRegistro(this.ctrNavigator1_MudaRegistroSelecionado);
            this.ctrNavigator1.EventoNovo += new ControlePacientes.Controles.CtrNavigator.Novo(this.ctrNavigator1_EventoNovo);
            this.ctrNavigator1.CancelarAcao += new ControlePacientes.Controles.CtrNavigator.Cancelar(this.ctrNavigator1_CancelarAcao);
            this.ctrNavigator1.EditarRegistro += new ControlePacientes.Controles.CtrNavigator.Editar(this.ctrNavigator1_EditarRegistro);
            this.ctrNavigator1.SalvarRegistro += new ControlePacientes.Controles.CtrNavigator.Salvar(this.ctrNavigator1_SalvarRegistro);
            this.ctrNavigator1.ExcluirRegistro += new ControlePacientes.Controles.CtrNavigator.Excluir(this.ctrNavigator1_ExcluirRegistro);
            // 
            // lblCaracteresRestantes
            // 
            this.lblCaracteresRestantes.AutoSize = true;
            this.lblCaracteresRestantes.Location = new System.Drawing.Point(411, 163);
            this.lblCaracteresRestantes.Name = "lblCaracteresRestantes";
            this.lblCaracteresRestantes.Size = new System.Drawing.Size(127, 13);
            this.lblCaracteresRestantes.TabIndex = 138;
            this.lblCaracteresRestantes.Text = "255 caracteres restantes.";
            // 
            // ColCodigo
            // 
            this.ColCodigo.DataPropertyName = "Codigo";
            this.ColCodigo.HeaderText = "Código";
            this.ColCodigo.Name = "ColCodigo";
            this.ColCodigo.ReadOnly = true;
            this.ColCodigo.Width = 60;
            // 
            // ColNome
            // 
            this.ColNome.DataPropertyName = "nomeExame";
            this.ColNome.HeaderText = "Exame";
            this.ColNome.Name = "ColNome";
            this.ColNome.ReadOnly = true;
            this.ColNome.Width = 190;
            // 
            // ColCNPJ
            // 
            this.ColCNPJ.DataPropertyName = "clinica";
            this.ColCNPJ.HeaderText = "Clinica";
            this.ColCNPJ.Name = "ColCNPJ";
            this.ColCNPJ.ReadOnly = true;
            this.ColCNPJ.Width = 150;
            // 
            // ColNomeFantasia
            // 
            this.ColNomeFantasia.DataPropertyName = "observacao";
            this.ColNomeFantasia.HeaderText = "Observação";
            this.ColNomeFantasia.Name = "ColNomeFantasia";
            this.ColNomeFantasia.ReadOnly = true;
            this.ColNomeFantasia.Width = 210;
            // 
            // CadastroExames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 532);
            this.Controls.Add(this.lblCaracteresRestantes);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.DGExames);
            this.Controls.Add(this.txtObservacao);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cmbClinica);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtNomeExame);
            this.Controls.Add(this.ctrNavigator1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastroExames";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de tipos Exames";
            this.Load += new System.EventHandler(this.CadastroExames_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGExames)).EndInit();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controles.CtrNavigator ctrNavigator1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtNomeExame;
        private System.Windows.Forms.ComboBox cmbClinica;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView DGExames;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel LblStatus;
        private System.Windows.Forms.Label lblCaracteresRestantes;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNome;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCNPJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNomeFantasia;
    }
}