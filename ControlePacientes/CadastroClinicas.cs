﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BubbleSort.Dominio;


namespace ControlePacientes
{
    public partial class CadastroClinicas : Form
    {
        public CadastroClinicas(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Repositorio = new BubbleSort.Repositorio.ClinicaRepositorio(session);
        }


        private Clinicas _Clinica;
        BindingSource _Binding;
        private List<Clinicas> _ListaClinicas;
        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private BubbleSort.Repositorio.ClinicaRepositorio _Repositorio;

        private void CadastroClinicas_Load(object sender, EventArgs e)
        {
            this.BuscarTodasAsClinicas();
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void ValidaDados()
        {
            if (TxtNomeClinica.Text.Trim() == String.Empty)
                throw new Exception("O campo Nome Clínica é obrigatório.");
            if (TxtCNPJ.Text.Trim() == String.Empty)
                throw new Exception("O campo CNPJ é obrigatório.");
            if (TxtTelefone.Text.Trim() == String.Empty)
                throw new Exception("O campo Telefone é obrigatório.");       
            _Clinica.Nome = TxtNomeClinica.Text;
            _Clinica.NomeFantasia = TxtNomeFantasia.Text;
            _Clinica.Cnpj = TxtCNPJ.Text;
            _Clinica.InscricaoEstadual = TxtInscricaoEstadual.Text;
            _Clinica.TelefoneClinica = TxtTelefone.Text;
            _Clinica.TelefoneFax = TxtTelefoneFax.Text;
            _Clinica.Endereco.Rua = TxtEndereço.Text;
            _Clinica.Endereco.Bairro = TxtBairro.Text;
            _Clinica.Endereco.Cep = TxtCep.Text;
            _Clinica.Endereco.Cidade = TxtCidade.Text;
            _Clinica.Endereco.Estado = TxtEstado.Text;
            _Clinica.Endereco.Numero = TxtEnderecoNumero.Text;
        }

        private void EnabledCampos(bool enabled)
        {
            foreach (Control ctr in this.Controls)
            {
                if (ctr is ComboBox)
                    ((ComboBox)ctr).Enabled = enabled;
                else if (ctr is TextBox)
                    ((TextBox)ctr).Enabled = enabled;
                else if (ctr is MaskedTextBox)
                    ((MaskedTextBox)ctr).Enabled = enabled;
                else if (ctr is Button)
                    ((Button)ctr).Enabled = enabled;
                else if (ctr is DateTimePicker)
                    ((DateTimePicker)ctr).Enabled = enabled;
            }
        }

        private void CamposInterface(Controles.CtrNavigator.Status status)
        {
            if (_Clinica == null)
                _Clinica = new Clinicas();
            txtCodigo.Text = _Clinica.Codigo.ToString();
            TxtNomeClinica.Text = _Clinica.Nome;
            TxtNomeFantasia.Text = _Clinica.NomeFantasia;
            TxtCNPJ.Text = _Clinica.Cnpj;
            TxtInscricaoEstadual.Text = _Clinica.InscricaoEstadual;
            TxtTelefone.Text = _Clinica.TelefoneClinica;
            TxtTelefoneFax.Text = _Clinica.TelefoneFax;
            TxtEndereço.Text = _Clinica.Endereco.Rua;
            TxtEnderecoNumero.Text = _Clinica.Endereco.Numero;
            TxtCep.Text = _Clinica.Endereco.Cep;
            TxtBairro.Text = _Clinica.Endereco.Bairro;
            TxtCidade.Text = _Clinica.Endereco.Cidade;
            TxtEstado.Text = _Clinica.Endereco.Estado;
            if (status == Controles.CtrNavigator.Status.Inserindo)
            {
                txtCodigo.Text = _Repositorio.ProximoCodigo().ToString();
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Inserindo";
            }
            else if (status == Controles.CtrNavigator.Status.Editando)
            {
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Editando";
            }
            else if (status == Controles.CtrNavigator.Status.Excluindo)
            {
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Excluindo";
            }
            else
            {
                this.ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Consultando";
            }
        }

        private void BuscarTodasAsClinicas()
        {
            _ListaClinicas = (List<BubbleSort.Dominio.Clinicas>)_Repositorio.Todos();
            _Binding = new BindingSource(_ListaClinicas, "");
            _Binding.ResetBindings(true);
            this.DGClinicas.AutoGenerateColumns = false;
            this.DGClinicas.DataSource = _Binding;
            ctrNavigator1.DataSource = _Binding;
        }

        private void ctrNavigator1_MudaRegistroSelecionado(object objetoAtual)
        {
            DGClinicas.Rows[ctrNavigator1.Indice].Selected = true;
        }

        private void ctrNavigator1_EventoNovo()
        {
            _Clinica = null;
            _Clinica = new Clinicas();
            this.CamposInterface(Controles.CtrNavigator.Status.Inserindo);
        }

        private void ctrNavigator1_EditarRegistro(object objEditar)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Editando);
        }

        private void ctrNavigator1_CancelarAcao()
        {
            if (DGClinicas.SelectedRows.Count > 0)
                _Clinica = (Clinicas)DGClinicas.SelectedRows[0].DataBoundItem;
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void ctrNavigator1_SalvarRegistro(object objSalvar)
        {
            try
            {
                this.ValidaDados();
                if (string.IsNullOrWhiteSpace(_Clinica.Id))
                {
                    _Repositorio.Adicionar(_Clinica);
                    _ListaClinicas.Add(_Clinica);
                }
                else
                    _Repositorio.Atualizar(_Clinica);
                _Binding.ResetBindings(true);
                ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
            } catch (Exception ex)
            {
                this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        private void ctrNavigator1_ExcluirRegistro(object objExcluir)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Excluindo);
            if (MessageBox.Show("Deseja excluir o registro.", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    _Repositorio.Deletar(_Clinica);
                    this._ListaClinicas.Remove(_Clinica);
                    this._Binding.ResetBindings(true);
                } catch (Exception ex)
                {
                    this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void DGClinicas_SelectionChanged(object sender, EventArgs e)
        {
            if (this.DGClinicas.Rows.Count > 0)
            {
                if (DGClinicas.SelectedRows.Count > 0)
                {
                    _Clinica = (Clinicas)DGClinicas.SelectedRows[0].DataBoundItem;
                    if (ctrNavigator1.DataSource != null)
                        ctrNavigator1.Indice = DGClinicas.SelectedRows[0].Index;
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }
    }
}
