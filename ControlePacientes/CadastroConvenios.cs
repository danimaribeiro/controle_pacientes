﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes
{
    public partial class CadastroConvenios : Form
    {
        private BubbleSort.Dominio.Convenios _Convenios;
        BindingSource _Binding;
        private List<BubbleSort.Dominio.Convenios> _ListaConvenios;
        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private BubbleSort.Repositorio.ConvenioRepositorio _Repositorio;
        private BubbleSort.Repositorio.ClinicaRepositorio _ClinicaRepositorio;

        public CadastroConvenios(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Repositorio = new BubbleSort.Repositorio.ConvenioRepositorio(session);
            _ClinicaRepositorio = new BubbleSort.Repositorio.ClinicaRepositorio(session);
        }

        private void CadastroConvenios_Load(object sender, EventArgs e)
        {
            this.BuscarTodosOsConvenios();
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void BuscarTodosOsConvenios()
        {
            _ListaConvenios = (List<BubbleSort.Dominio.Convenios>)_Repositorio.Todos();
            _Binding = new BindingSource(_ListaConvenios, "");
            _Binding.ResetBindings(true);
            this.gdvConvenios.AutoGenerateColumns = false;
            this.gdvConvenios.DataSource = _Binding;
            ctrNavigator1.DataSource = _Binding;
            var lista = (List<BubbleSort.Dominio.Clinicas>)_ClinicaRepositorio.Todos();
            lista.Insert(0, new BubbleSort.Dominio.Clinicas());
            cmbClinica.DataSource = lista;
            cmbClinica.SelectedIndex = 0;
        }

        private void CamposInterface(Controles.CtrNavigator.Status status)
        {
            if (_Convenios == null)
                _Convenios = new BubbleSort.Dominio.Convenios();
            txtCodigo.Text = _Convenios.Codigo.ToString();
            TxtNomeConvenio.Text = _Convenios.nomeConvenio;
            txtObservacao.Text = _Convenios.observacao;
            this.cmbClinica.SelectedItem = _Convenios.clinica;
            if (status == Controles.CtrNavigator.Status.Inserindo)
            {
                txtCodigo.Text = _Repositorio.ProximoCodigo().ToString();
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Inserindo";
            }
            else if (status == Controles.CtrNavigator.Status.Editando)
            {
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Editando";
            }
            else if (status == Controles.CtrNavigator.Status.Excluindo)
            {
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Excluindo";
            }
            else
            {
                this.ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Consultando";
            }
        }

        private void EnabledCampos(bool enabled)
        {
            foreach (Control ctr in this.Controls)
            {
                if (ctr is ComboBox)
                    ((ComboBox)ctr).Enabled = enabled;
                else if (ctr is TextBox)
                    ((TextBox)ctr).Enabled = enabled;
                else if (ctr is MaskedTextBox)
                    ((MaskedTextBox)ctr).Enabled = enabled;
                else if (ctr is Button)
                    ((Button)ctr).Enabled = enabled;
                else if (ctr is DateTimePicker)
                    ((DateTimePicker)ctr).Enabled = enabled;
            }
        }

        private void ctrNavigator1_CancelarAcao()
        {
            if (gdvConvenios.SelectedRows.Count > 0)
                _Convenios = (BubbleSort.Dominio.Convenios)gdvConvenios.SelectedRows[0].DataBoundItem;
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void ctrNavigator1_EditarRegistro(object objEditar)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Editando);
        }

        private void ctrNavigator1_EventoNovo()
        {
            _Convenios = null;
            _Convenios = new BubbleSort.Dominio.Convenios();
            this.CamposInterface(Controles.CtrNavigator.Status.Inserindo);
        }

        private void ctrNavigator1_ExcluirRegistro(object objExcluir)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Excluindo);
            if (MessageBox.Show("Deseja excluir o registro.", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    _Repositorio.Deletar(_Convenios);
                    this._ListaConvenios.Remove(_Convenios);
                    this._Binding.ResetBindings(true);
                } catch (Exception ex)
                {
                    this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void ctrNavigator1_MudaRegistroSelecionado(object objetoAtual)
        {
            gdvConvenios.Rows[ctrNavigator1.Indice].Selected = true;
        }

        private void ctrNavigator1_SalvarRegistro(object objSalvar)
        {
            try
            {
                this.ValidaDados();
                if (string.IsNullOrWhiteSpace(_Convenios.Id))
                {
                    _Repositorio.Adicionar(_Convenios);
                    _ListaConvenios.Add(_Convenios);
                }
                else
                    _Repositorio.Atualizar(_Convenios);
                _Binding.ResetBindings(true);
                ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
            } catch (Exception ex)
            {
                this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        private void ValidaDados()
        {
            if (string.IsNullOrWhiteSpace(TxtNomeConvenio.Text))
                throw new Exception("O campo Nome Convenio é obrigatório.");
            _Convenios.clinica = cmbClinica.SelectedIndex > 0 ? (BubbleSort.Dominio.Clinicas)cmbClinica.SelectedItem : null;
            _Convenios.nomeConvenio = TxtNomeConvenio.Text;
            _Convenios.observacao = txtObservacao.Text;
        }

        private void gdvConvenios_SelectionChanged(object sender, EventArgs e)
        {
            if (this.gdvConvenios.Rows.Count > 0)
            {
                if (gdvConvenios.SelectedRows.Count > 0)
                {
                    _Convenios = (BubbleSort.Dominio.Convenios)gdvConvenios.SelectedRows[0].DataBoundItem;
                    if (ctrNavigator1.DataSource != null)
                        ctrNavigator1.Indice = gdvConvenios.SelectedRows[0].Index;
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void txtObservacao_TextChanged(object sender, EventArgs e)
        {
            lblCaracteresRestantes.Text = (txtObservacao.MaxLength - txtObservacao.Text.Length) + " caracteres restantes.";
        }
    }
}
