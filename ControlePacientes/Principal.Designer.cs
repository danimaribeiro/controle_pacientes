﻿namespace ControlePacientes
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastrosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeClínicasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeConvêniosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeExamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDePacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeProfissionaisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasDePacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosDePacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDeConsultasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDeConsultaDetalhadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logotipoRelatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clinicasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profissionaisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convêniosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.examesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasDoPacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDePacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDeConsultasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAtualizarConsultas = new System.Windows.Forms.Button();
            this.gdvConsultasHoje = new System.Windows.Forms.DataGridView();
            this.ColData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPaciente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colModalidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.LblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.TSAtalhos = new System.Windows.Forms.ToolStrip();
            this.toolStripProfissionais = new System.Windows.Forms.ToolStripButton();
            this.toolStripCadastroPacientes = new System.Windows.Forms.ToolStripButton();
            this.toolStripCadastroConsulta = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn4 = new System.Windows.Forms.DataGridViewImageColumn();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdvConsultasHoje)).BeginInit();
            this.TSAtalhos.SuspendLayout();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem1,
            this.relatóriosToolStripMenuItem1,
            this.configuraçõesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(889, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastrosToolStripMenuItem1
            // 
            this.cadastrosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroDeClínicasToolStripMenuItem,
            this.cadastroDeConvêniosToolStripMenuItem,
            this.cadastroDeExamesToolStripMenuItem,
            this.cadastroDePacientesToolStripMenuItem,
            this.cadastroDeProfissionaisToolStripMenuItem,
            this.consultasDePacientesToolStripMenuItem});
            this.cadastrosToolStripMenuItem1.Name = "cadastrosToolStripMenuItem1";
            this.cadastrosToolStripMenuItem1.Size = new System.Drawing.Size(71, 20);
            this.cadastrosToolStripMenuItem1.Text = "Cadastros";
            // 
            // cadastroDeClínicasToolStripMenuItem
            // 
            this.cadastroDeClínicasToolStripMenuItem.Name = "cadastroDeClínicasToolStripMenuItem";
            this.cadastroDeClínicasToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.cadastroDeClínicasToolStripMenuItem.Text = "Cadastro de Clínicas";
            this.cadastroDeClínicasToolStripMenuItem.Click += new System.EventHandler(this.clinicasToolStripMenuItem_Click);
            // 
            // cadastroDeConvêniosToolStripMenuItem
            // 
            this.cadastroDeConvêniosToolStripMenuItem.Name = "cadastroDeConvêniosToolStripMenuItem";
            this.cadastroDeConvêniosToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.cadastroDeConvêniosToolStripMenuItem.Text = "Cadastro de Convênios";
            this.cadastroDeConvêniosToolStripMenuItem.Click += new System.EventHandler(this.convêniosToolStripMenuItem_Click);
            // 
            // cadastroDeExamesToolStripMenuItem
            // 
            this.cadastroDeExamesToolStripMenuItem.Name = "cadastroDeExamesToolStripMenuItem";
            this.cadastroDeExamesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.cadastroDeExamesToolStripMenuItem.Text = "Cadastro de Exames";
            this.cadastroDeExamesToolStripMenuItem.Click += new System.EventHandler(this.examesToolStripMenuItem_Click);
            // 
            // cadastroDePacientesToolStripMenuItem
            // 
            this.cadastroDePacientesToolStripMenuItem.Name = "cadastroDePacientesToolStripMenuItem";
            this.cadastroDePacientesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.cadastroDePacientesToolStripMenuItem.Text = "Cadastro de Pacientes";
            this.cadastroDePacientesToolStripMenuItem.Click += new System.EventHandler(this.pacientesToolStripMenuItem_Click);
            // 
            // cadastroDeProfissionaisToolStripMenuItem
            // 
            this.cadastroDeProfissionaisToolStripMenuItem.Name = "cadastroDeProfissionaisToolStripMenuItem";
            this.cadastroDeProfissionaisToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.cadastroDeProfissionaisToolStripMenuItem.Text = "Cadastro de Profissionais";
            this.cadastroDeProfissionaisToolStripMenuItem.Click += new System.EventHandler(this.profissionaisToolStripMenuItem_Click);
            // 
            // consultasDePacientesToolStripMenuItem
            // 
            this.consultasDePacientesToolStripMenuItem.Name = "consultasDePacientesToolStripMenuItem";
            this.consultasDePacientesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.consultasDePacientesToolStripMenuItem.Text = "Consultas de Pacientes";
            this.consultasDePacientesToolStripMenuItem.Click += new System.EventHandler(this.consultasDoPacienteToolStripMenuItem_Click);
            // 
            // relatóriosToolStripMenuItem1
            // 
            this.relatóriosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.relatóriosDePacientesToolStripMenuItem,
            this.relatórioDeConsultasToolStripMenuItem1,
            this.relatórioDeConsultaDetalhadoToolStripMenuItem});
            this.relatóriosToolStripMenuItem1.Name = "relatóriosToolStripMenuItem1";
            this.relatóriosToolStripMenuItem1.Size = new System.Drawing.Size(71, 20);
            this.relatóriosToolStripMenuItem1.Text = "Relatórios";
            // 
            // relatóriosDePacientesToolStripMenuItem
            // 
            this.relatóriosDePacientesToolStripMenuItem.Name = "relatóriosDePacientesToolStripMenuItem";
            this.relatóriosDePacientesToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.relatóriosDePacientesToolStripMenuItem.Text = "Relatórios de Pacientes";
            this.relatóriosDePacientesToolStripMenuItem.Click += new System.EventHandler(this.relatórioDePacientesToolStripMenuItem_Click);
            // 
            // relatórioDeConsultasToolStripMenuItem1
            // 
            this.relatórioDeConsultasToolStripMenuItem1.Name = "relatórioDeConsultasToolStripMenuItem1";
            this.relatórioDeConsultasToolStripMenuItem1.Size = new System.Drawing.Size(244, 22);
            this.relatórioDeConsultasToolStripMenuItem1.Text = "Relatório de Consultas";
            this.relatórioDeConsultasToolStripMenuItem1.Click += new System.EventHandler(this.relatórioDeConsultasToolStripMenuItem_Click);
            // 
            // relatórioDeConsultaDetalhadoToolStripMenuItem
            // 
            this.relatórioDeConsultaDetalhadoToolStripMenuItem.Name = "relatórioDeConsultaDetalhadoToolStripMenuItem";
            this.relatórioDeConsultaDetalhadoToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.relatórioDeConsultaDetalhadoToolStripMenuItem.Text = "Relatório de Consulta Detalhado";
            this.relatórioDeConsultaDetalhadoToolStripMenuItem.Click += new System.EventHandler(this.relatórioDeConsultaDetalhadoToolStripMenuItem_Click);
            // 
            // configuraçõesToolStripMenuItem
            // 
            this.configuraçõesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logotipoRelatóriosToolStripMenuItem,
            this.sincronizarToolStripMenuItem});
            this.configuraçõesToolStripMenuItem.Name = "configuraçõesToolStripMenuItem";
            this.configuraçõesToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.configuraçõesToolStripMenuItem.Text = "Configurações";
            // 
            // logotipoRelatóriosToolStripMenuItem
            // 
            this.logotipoRelatóriosToolStripMenuItem.Name = "logotipoRelatóriosToolStripMenuItem";
            this.logotipoRelatóriosToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.logotipoRelatóriosToolStripMenuItem.Text = "Logotipo relatórios";
            this.logotipoRelatóriosToolStripMenuItem.Click += new System.EventHandler(this.logotipoRelatóriosToolStripMenuItem_Click);
            // 
            // sincronizarToolStripMenuItem
            // 
            this.sincronizarToolStripMenuItem.Name = "sincronizarToolStripMenuItem";
            this.sincronizarToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.sincronizarToolStripMenuItem.Text = "Sincronizar";
            this.sincronizarToolStripMenuItem.Click += new System.EventHandler(this.sincronizarToolStripMenuItem_Click);
            // 
            // sistemaToolStripMenuItem
            // 
            this.sistemaToolStripMenuItem.Name = "sistemaToolStripMenuItem";
            this.sistemaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.sistemaToolStripMenuItem.Text = "Sistema";
            // 
            // cadastrosToolStripMenuItem
            // 
            this.cadastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clinicasToolStripMenuItem,
            this.pacientesToolStripMenuItem,
            this.profissionaisToolStripMenuItem,
            this.convêniosToolStripMenuItem,
            this.examesToolStripMenuItem,
            this.consultasDoPacienteToolStripMenuItem});
            this.cadastrosToolStripMenuItem.Name = "cadastrosToolStripMenuItem";
            this.cadastrosToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.cadastrosToolStripMenuItem.Text = "Cadastros";
            // 
            // clinicasToolStripMenuItem
            // 
            this.clinicasToolStripMenuItem.Name = "clinicasToolStripMenuItem";
            this.clinicasToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.clinicasToolStripMenuItem.Text = "Clinicas";
            this.clinicasToolStripMenuItem.Click += new System.EventHandler(this.clinicasToolStripMenuItem_Click);
            // 
            // pacientesToolStripMenuItem
            // 
            this.pacientesToolStripMenuItem.Name = "pacientesToolStripMenuItem";
            this.pacientesToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.pacientesToolStripMenuItem.Text = "Pacientes";
            this.pacientesToolStripMenuItem.Click += new System.EventHandler(this.pacientesToolStripMenuItem_Click);
            // 
            // profissionaisToolStripMenuItem
            // 
            this.profissionaisToolStripMenuItem.Name = "profissionaisToolStripMenuItem";
            this.profissionaisToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.profissionaisToolStripMenuItem.Text = "Profissionais";
            this.profissionaisToolStripMenuItem.Click += new System.EventHandler(this.profissionaisToolStripMenuItem_Click);
            // 
            // convêniosToolStripMenuItem
            // 
            this.convêniosToolStripMenuItem.Name = "convêniosToolStripMenuItem";
            this.convêniosToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.convêniosToolStripMenuItem.Text = "Convênios";
            this.convêniosToolStripMenuItem.Click += new System.EventHandler(this.convêniosToolStripMenuItem_Click);
            // 
            // examesToolStripMenuItem
            // 
            this.examesToolStripMenuItem.Name = "examesToolStripMenuItem";
            this.examesToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.examesToolStripMenuItem.Text = "Exames";
            this.examesToolStripMenuItem.Click += new System.EventHandler(this.examesToolStripMenuItem_Click);
            // 
            // consultasDoPacienteToolStripMenuItem
            // 
            this.consultasDoPacienteToolStripMenuItem.Name = "consultasDoPacienteToolStripMenuItem";
            this.consultasDoPacienteToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.consultasDoPacienteToolStripMenuItem.Text = "Consultas do Paciente";
            this.consultasDoPacienteToolStripMenuItem.Click += new System.EventHandler(this.consultasDoPacienteToolStripMenuItem_Click);
            // 
            // relatóriosToolStripMenuItem
            // 
            this.relatóriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.relatórioDePacientesToolStripMenuItem,
            this.relatórioDeConsultasToolStripMenuItem});
            this.relatóriosToolStripMenuItem.Name = "relatóriosToolStripMenuItem";
            this.relatóriosToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.relatóriosToolStripMenuItem.Text = "Relatórios";
            // 
            // relatórioDePacientesToolStripMenuItem
            // 
            this.relatórioDePacientesToolStripMenuItem.Name = "relatórioDePacientesToolStripMenuItem";
            this.relatórioDePacientesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.relatórioDePacientesToolStripMenuItem.Text = "Relatório de Pacientes";
            this.relatórioDePacientesToolStripMenuItem.Click += new System.EventHandler(this.relatórioDePacientesToolStripMenuItem_Click);
            // 
            // relatórioDeConsultasToolStripMenuItem
            // 
            this.relatórioDeConsultasToolStripMenuItem.Name = "relatórioDeConsultasToolStripMenuItem";
            this.relatórioDeConsultasToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.relatórioDeConsultasToolStripMenuItem.Text = "Relatório de Consultas";
            this.relatórioDeConsultasToolStripMenuItem.Click += new System.EventHandler(this.relatórioDeConsultasToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnAtualizarConsultas);
            this.groupBox1.Controls.Add(this.gdvConsultasHoje);
            this.groupBox1.Location = new System.Drawing.Point(12, 91);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(856, 388);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consultas de hoje";
            // 
            // btnAtualizarConsultas
            // 
            this.btnAtualizarConsultas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAtualizarConsultas.Image = global::ControlePacientes.Properties.Resources.atualizar;
            this.btnAtualizarConsultas.Location = new System.Drawing.Point(810, 8);
            this.btnAtualizarConsultas.Name = "btnAtualizarConsultas";
            this.btnAtualizarConsultas.Size = new System.Drawing.Size(29, 23);
            this.btnAtualizarConsultas.TabIndex = 63;
            this.btnAtualizarConsultas.UseVisualStyleBackColor = true;
            this.btnAtualizarConsultas.Click += new System.EventHandler(this.btnAtualizarConsultas_Click);
            // 
            // gdvConsultasHoje
            // 
            this.gdvConsultasHoje.AllowUserToAddRows = false;
            this.gdvConsultasHoje.AllowUserToDeleteRows = false;
            this.gdvConsultasHoje.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gdvConsultasHoje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdvConsultasHoje.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColData,
            this.ColPaciente,
            this.colModalidade,
            this.Column1,
            this.Column2});
            this.gdvConsultasHoje.Location = new System.Drawing.Point(18, 33);
            this.gdvConsultasHoje.Name = "gdvConsultasHoje";
            this.gdvConsultasHoje.ReadOnly = true;
            this.gdvConsultasHoje.RowHeadersWidth = 20;
            this.gdvConsultasHoje.Size = new System.Drawing.Size(821, 339);
            this.gdvConsultasHoje.TabIndex = 62;
            this.gdvConsultasHoje.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdvConsultasHoje_CellClick);
            // 
            // ColData
            // 
            this.ColData.DataPropertyName = "Data";
            this.ColData.HeaderText = "Data";
            this.ColData.Name = "ColData";
            this.ColData.ReadOnly = true;
            this.ColData.Width = 80;
            // 
            // ColPaciente
            // 
            this.ColPaciente.DataPropertyName = "NomePaciente";
            this.ColPaciente.HeaderText = "Paciente";
            this.ColPaciente.Name = "ColPaciente";
            this.ColPaciente.ReadOnly = true;
            this.ColPaciente.Width = 380;
            // 
            // colModalidade
            // 
            this.colModalidade.DataPropertyName = "Tipo";
            this.colModalidade.HeaderText = "Modalidade";
            this.colModalidade.Name = "colModalidade";
            this.colModalidade.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Visualizar";
            this.Column1.Image = global::ControlePacientes.Properties.Resources.consulta_45;
            this.Column1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Paciente";
            this.Column2.Image = global::ControlePacientes.Properties.Resources.paciente_45;
            this.Column2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // LblStatus
            // 
            this.LblStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(124, 17);
            this.LblStatus.Text = "Status : Consultando";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "Visualizar";
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "Paciente";
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(57, 57);
            this.toolStripButton1.Text = "toolStripProfissional";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 57);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 57);
            this.toolStripButton3.Text = "toolStripButton3";
            // 
            // TSAtalhos
            // 
            this.TSAtalhos.AutoSize = false;
            this.TSAtalhos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProfissionais,
            this.toolStripCadastroPacientes,
            this.toolStripCadastroConsulta});
            this.TSAtalhos.Location = new System.Drawing.Point(0, 24);
            this.TSAtalhos.Name = "TSAtalhos";
            this.TSAtalhos.Size = new System.Drawing.Size(889, 60);
            this.TSAtalhos.TabIndex = 129;
            this.TSAtalhos.Text = "toolStrip1";
            // 
            // toolStripProfissionais
            // 
            this.toolStripProfissionais.AutoSize = false;
            this.toolStripProfissionais.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripProfissionais.Image = global::ControlePacientes.Properties.Resources.profissional_45;
            this.toolStripProfissionais.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripProfissionais.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripProfissionais.Name = "toolStripProfissionais";
            this.toolStripProfissionais.Size = new System.Drawing.Size(57, 57);
            this.toolStripProfissionais.Text = "Cadastro de Profissionais";
            this.toolStripProfissionais.Click += new System.EventHandler(this.toolStripProfissionais_Click);
            // 
            // toolStripCadastroPacientes
            // 
            this.toolStripCadastroPacientes.AutoSize = false;
            this.toolStripCadastroPacientes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCadastroPacientes.Image = global::ControlePacientes.Properties.Resources.paciente_45;
            this.toolStripCadastroPacientes.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripCadastroPacientes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCadastroPacientes.Name = "toolStripCadastroPacientes";
            this.toolStripCadastroPacientes.Size = new System.Drawing.Size(57, 57);
            this.toolStripCadastroPacientes.Text = "Cadastro de Pacientes";
            this.toolStripCadastroPacientes.Click += new System.EventHandler(this.toolStripCadastroPacientes_Click);
            // 
            // toolStripCadastroConsulta
            // 
            this.toolStripCadastroConsulta.AutoSize = false;
            this.toolStripCadastroConsulta.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCadastroConsulta.Image = global::ControlePacientes.Properties.Resources.consulta_45;
            this.toolStripCadastroConsulta.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripCadastroConsulta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCadastroConsulta.Name = "toolStripCadastroConsulta";
            this.toolStripCadastroConsulta.Size = new System.Drawing.Size(57, 57);
            this.toolStripCadastroConsulta.Text = "Cadastro de Consultas";
            this.toolStripCadastroConsulta.Click += new System.EventHandler(this.toolStripCadastroConsulta_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.AutoSize = false;
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 57);
            this.toolStripButton4.Text = "toolStripProfissional";
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.StatusStrip.Location = new System.Drawing.Point(0, 482);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(889, 22);
            this.StatusStrip.TabIndex = 130;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(124, 17);
            this.toolStripStatusLabel1.Text = "Status : Consultando";
            // 
            // dataGridViewImageColumn3
            // 
            this.dataGridViewImageColumn3.HeaderText = "Visualizar";
            this.dataGridViewImageColumn3.Image = global::ControlePacientes.Properties.Resources.consulta_45;
            this.dataGridViewImageColumn3.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn3.Name = "dataGridViewImageColumn3";
            this.dataGridViewImageColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn4
            // 
            this.dataGridViewImageColumn4.HeaderText = "Paciente";
            this.dataGridViewImageColumn4.Image = global::ControlePacientes.Properties.Resources.paciente_45;
            this.dataGridViewImageColumn4.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn4.Name = "dataGridViewImageColumn4";
            this.dataGridViewImageColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.AutoSize = false;
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = global::ControlePacientes.Properties.Resources.paciente_45;
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(57, 57);
            this.toolStripButton5.Text = "toolStripButton5";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 504);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.TSAtalhos);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdvConsultasHoje)).EndInit();
            this.TSAtalhos.ResumeLayout(false);
            this.TSAtalhos.PerformLayout();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sistemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clinicasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profissionaisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convêniosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem examesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultasDoPacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatórioDePacientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatórioDeConsultasToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripStatusLabel LblStatus;
        private System.Windows.Forms.DataGridView gdvConsultasHoje;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStrip TSAtalhos;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripCadastroConsulta;
        private System.Windows.Forms.ToolStripButton toolStripCadastroPacientes;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeClínicasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeConvêniosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeExamesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDePacientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeProfissionaisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultasDePacientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton toolStripProfissionais;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn3;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn4;
        private System.Windows.Forms.ToolStripMenuItem relatóriosDePacientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatórioDeConsultasToolStripMenuItem1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColData;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPaciente;
        private System.Windows.Forms.DataGridViewTextBoxColumn colModalidade;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewImageColumn Column2;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem relatórioDeConsultaDetalhadoToolStripMenuItem;
        private System.Windows.Forms.Button btnAtualizarConsultas;
        private System.Windows.Forms.ToolStripMenuItem configuraçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logotipoRelatóriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarToolStripMenuItem;
    }
}