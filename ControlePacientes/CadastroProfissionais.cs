﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BubbleSort.Dominio;

namespace ControlePacientes
{
    public partial class CadastroProfissionais : Form
    {
        public CadastroProfissionais(BubbleSort.Servicos.TratadorDeErro tratadorErro, NHibernate.ISession session)
        {
            InitializeComponent();
            _TratadorDeErro = tratadorErro;
            _Repositorio = new BubbleSort.Repositorio.ProfissionalRepositorio(session);
            _RepositorioClinica = new BubbleSort.Repositorio.ClinicaRepositorio(session);
        }

        private Profissionais _Profissional;
        BindingSource _Binding;
        private List<Profissionais> _ListaProfissionais;
        private BubbleSort.Servicos.TratadorDeErro _TratadorDeErro;
        private BubbleSort.Repositorio.ProfissionalRepositorio _Repositorio;
        private BubbleSort.Repositorio.ClinicaRepositorio _RepositorioClinica;

        private void CadastroProfissionais_Load(object sender, EventArgs e)
        {
            this.BuscarTodasAsClinicas();
            this.BuscarTodosOsProfissionais();
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void BuscarTodasAsClinicas()
        {
            var lista = (List<BubbleSort.Dominio.Clinicas>)_RepositorioClinica.Todos();
            lista.Insert(0, new Clinicas());
            this.cmbClinica.DataSource = lista;
            this.cmbClinica.SelectedIndex = 0;
        }

        private void ValidaDados()
        {
            if (TxtNome.Text.Trim() == String.Empty)
                throw new Exception("O campo Nome é obrigatório.");
            if (TxtCPFCNPJ.Text.Trim() == String.Empty)
                throw new Exception("O campo CPF é obrigatório.");
            if (TxtTelefone.Text.Trim() == String.Empty)
                throw new Exception("O campo Telefone é obrigatório.");
            _Profissional.Nome = TxtNome.Text;
            _Profissional.Email = TxtEmail.Text;
            _Profissional.Cpf = TxtCPFCNPJ.Text;
            _Profissional.Rg = TxtRG_InscricaoEstadual.Text;
            _Profissional.TelefoneFixo = TxtTelefone.Text;
            _Profissional.TelefoneCelular = TxtTelefoneCelular.Text;
            _Profissional.Observacao = txtObservacao.Text;
            _Profissional.DataNascimento = dateTimePicker1.Value;
            _Profissional.Clinica = cmbClinica.SelectedIndex > 0 ? (Clinicas)cmbClinica.SelectedItem : null;
            _Profissional.Endereco.Rua = TxtEndereço.Text;
            _Profissional.Endereco.Bairro = TxtBairro.Text;
            _Profissional.Endereco.Cep = TxtCep.Text;
            _Profissional.Endereco.Cidade = TxtCidade.Text;
            _Profissional.Endereco.Estado = TxtEstado.Text;
            _Profissional.Endereco.Numero = TxtEnderecoNumero.Text;
        }

        private void EnabledCampos(bool enabled)
        {
            foreach (Control ctr in this.Controls)
            {
                if (ctr is ComboBox)
                    ((ComboBox)ctr).Enabled = enabled;
                else if (ctr is TextBox)
                    ((TextBox)ctr).Enabled = enabled;
                else if (ctr is MaskedTextBox)
                    ((MaskedTextBox)ctr).Enabled = enabled;
                else if (ctr is Button)
                    ((Button)ctr).Enabled = enabled;
                else if (ctr is DateTimePicker)
                    ((DateTimePicker)ctr).Enabled = enabled;
            }
        }

        private void CamposInterface(Controles.CtrNavigator.Status status)
        {
            if (_Profissional == null)
                _Profissional = new Profissionais();
            txtCodigo.Text = _Profissional.Codigo.ToString();
            TxtNome.Text = _Profissional.Nome;
            TxtEmail.Text = _Profissional.Email;
            TxtCPFCNPJ.Text = _Profissional.Cpf;
            TxtRG_InscricaoEstadual.Text = _Profissional.Rg;
            TxtTelefone.Text = _Profissional.TelefoneFixo;
            TxtTelefoneCelular.Text = _Profissional.TelefoneCelular;
            TxtEndereço.Text = _Profissional.Endereco.Rua;
            TxtEnderecoNumero.Text = _Profissional.Endereco.Numero;
            TxtCep.Text = _Profissional.Endereco.Cep;
            TxtBairro.Text = _Profissional.Endereco.Bairro;
            TxtCidade.Text = _Profissional.Endereco.Cidade;
            TxtEstado.Text = _Profissional.Endereco.Estado;
            txtObservacao.Text = _Profissional.Observacao;
            cmbClinica.SelectedItem = _Profissional.Clinica;
            if (status == Controles.CtrNavigator.Status.Inserindo)
            {
                txtCodigo.Text = _Repositorio.ProximoCodigo().ToString();
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Inserindo";
            }
            else if (status == Controles.CtrNavigator.Status.Editando)
            {
                this.EnabledCampos(true);
                LblStatus.Text = "Status : Editando";
            }
            else if (status == Controles.CtrNavigator.Status.Excluindo)
            {
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Excluindo";
            }
            else
            {
                this.ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.EnabledCampos(false);
                LblStatus.Text = "Status : Consultando";
            }
        }

        private void BuscarTodosOsProfissionais()
        {
            _ListaProfissionais = (List<BubbleSort.Dominio.Profissionais>)_Repositorio.Todos();
            _Binding = new BindingSource(_ListaProfissionais, "");
            _Binding.ResetBindings(true);
            this.DGProfissionais.AutoGenerateColumns = false;
            this.DGProfissionais.DataSource = _Binding;
            ctrNavigator1.DataSource = _Binding;
        }

        private void ctrNavigator1_MudaRegistroSelecionado(object objetoAtual)
        {
            DGProfissionais.Rows[ctrNavigator1.Indice].Selected = true;
        }

        private void ctrNavigator1_EventoNovo()
        {
            _Profissional = null;
            _Profissional = new Profissionais();
            this.CamposInterface(Controles.CtrNavigator.Status.Inserindo);
        }

        private void ctrNavigator1_EditarRegistro(object objEditar)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Editando);
        }

        private void ctrNavigator1_CancelarAcao()
        {
            if (DGProfissionais.SelectedRows.Count > 0)
                _Profissional = (Profissionais)DGProfissionais.SelectedRows[0].DataBoundItem;
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void ctrNavigator1_SalvarRegistro(object objSalvar)
        {
            try
            {
                this.ValidaDados();
                if (string.IsNullOrWhiteSpace(_Profissional.Id))
                {
                    _Repositorio.Adicionar(_Profissional);
                    _ListaProfissionais.Add(_Profissional);
                }
                else
                    _Repositorio.Atualizar(_Profissional);
                _Binding.ResetBindings(true);
                ctrNavigator1.EnabledButons(Controles.CtrNavigator.Status.Consultando);
                this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
            } catch (Exception ex)
            {
                this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        private void ctrNavigator1_ExcluirRegistro(object objExcluir)
        {
            this.CamposInterface(Controles.CtrNavigator.Status.Excluindo);
            if (MessageBox.Show("Deseja excluir o registro.", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    _Repositorio.Deletar(_Profissional);
                    this._ListaProfissionais.Remove(_Profissional);
                    this._Binding.ResetBindings(true);
                } catch (Exception ex)
                {
                    this._TratadorDeErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void DGProfissionais_SelectionChanged_1(object sender, EventArgs e)
        {
            if (this.DGProfissionais.Rows.Count > 0)
            {
                if (DGProfissionais.SelectedRows.Count > 0)
                {
                    _Profissional = (Profissionais)DGProfissionais.SelectedRows[0].DataBoundItem;
                    if (ctrNavigator1.DataSource != null)
                        ctrNavigator1.Indice = DGProfissionais.SelectedRows[0].Index;
                }
            }
            this.CamposInterface(Controles.CtrNavigator.Status.Consultando);
        }

        private void txtObservacao_TextChanged(object sender, EventArgs e)
        {
            lblCaracteresRestantes.Text = (txtObservacao.MaxLength - txtObservacao.Text.Length) + " caracteres restantes.";
        }

    }
}
