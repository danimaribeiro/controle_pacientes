﻿namespace ControlePacientes
{
    partial class CadastroPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroPaciente));
            this.DGPacientes = new System.Windows.Forms.DataGridView();
            this.ColCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTelefoneClinica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColTelefoneFax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColInscricaoEstadual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNomeFantasia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColEndereco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.TxtCep = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtTelefoneCelular = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCPFCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.lblRG_InscricaoEstadual = new System.Windows.Forms.Label();
            this.TxtRG_InscricaoEstadual = new System.Windows.Forms.TextBox();
            this.lblCPF_CNPJ = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtEstado = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCidade = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtBairro = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtEnderecoNumero = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtEndereço = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtNome = new System.Windows.Forms.TextBox();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.LblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.cmbClinica = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbProfissional = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbConvenio = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lblCaracteresRestantes = new System.Windows.Forms.Label();
            this.ctrNavigator1 = new ControlePacientes.Controles.CtrNavigator();
            ((System.ComponentModel.ISupportInitialize)(this.DGPacientes)).BeginInit();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // DGPacientes
            // 
            this.DGPacientes.AllowUserToAddRows = false;
            this.DGPacientes.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DGPacientes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGPacientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGPacientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColCodigo,
            this.ColNome,
            this.ColTelefoneClinica,
            this.ColTelefoneFax,
            this.ColCNPJ,
            this.ColInscricaoEstadual,
            this.ColNomeFantasia,
            this.ColEndereco});
            this.DGPacientes.Location = new System.Drawing.Point(12, 421);
            this.DGPacientes.MultiSelect = false;
            this.DGPacientes.Name = "DGPacientes";
            this.DGPacientes.ReadOnly = true;
            this.DGPacientes.RowHeadersWidth = 15;
            this.DGPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGPacientes.Size = new System.Drawing.Size(798, 219);
            this.DGPacientes.TabIndex = 17;
            this.DGPacientes.SelectionChanged += new System.EventHandler(this.DGClinicas_SelectionChanged);
            // 
            // ColCodigo
            // 
            this.ColCodigo.DataPropertyName = "Codigo";
            this.ColCodigo.HeaderText = "Código";
            this.ColCodigo.Name = "ColCodigo";
            this.ColCodigo.ReadOnly = true;
            this.ColCodigo.Width = 60;
            // 
            // ColNome
            // 
            this.ColNome.DataPropertyName = "Nome";
            this.ColNome.HeaderText = "Nome";
            this.ColNome.Name = "ColNome";
            this.ColNome.ReadOnly = true;
            this.ColNome.Width = 190;
            // 
            // ColTelefoneClinica
            // 
            this.ColTelefoneClinica.DataPropertyName = "TelefoneFixo";
            this.ColTelefoneClinica.HeaderText = "Telefone";
            this.ColTelefoneClinica.Name = "ColTelefoneClinica";
            this.ColTelefoneClinica.ReadOnly = true;
            // 
            // ColTelefoneFax
            // 
            this.ColTelefoneFax.DataPropertyName = "TelefoneCelular";
            this.ColTelefoneFax.HeaderText = "Celular";
            this.ColTelefoneFax.Name = "ColTelefoneFax";
            this.ColTelefoneFax.ReadOnly = true;
            // 
            // ColCNPJ
            // 
            this.ColCNPJ.DataPropertyName = "CPF";
            this.ColCNPJ.HeaderText = "CPF";
            this.ColCNPJ.Name = "ColCNPJ";
            this.ColCNPJ.ReadOnly = true;
            this.ColCNPJ.Width = 40;
            // 
            // ColInscricaoEstadual
            // 
            this.ColInscricaoEstadual.DataPropertyName = "RG";
            this.ColInscricaoEstadual.HeaderText = "RG";
            this.ColInscricaoEstadual.Name = "ColInscricaoEstadual";
            this.ColInscricaoEstadual.ReadOnly = true;
            this.ColInscricaoEstadual.Width = 160;
            // 
            // ColNomeFantasia
            // 
            this.ColNomeFantasia.DataPropertyName = "Email";
            this.ColNomeFantasia.HeaderText = "Email";
            this.ColNomeFantasia.Name = "ColNomeFantasia";
            this.ColNomeFantasia.ReadOnly = true;
            this.ColNomeFantasia.Width = 190;
            // 
            // ColEndereco
            // 
            this.ColEndereco.DataPropertyName = "Endereco";
            this.ColEndereco.HeaderText = "Endereço";
            this.ColEndereco.Name = "ColEndereco";
            this.ColEndereco.ReadOnly = true;
            this.ColEndereco.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(74, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 15);
            this.label13.TabIndex = 81;
            this.label13.Text = "Código :";
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.White;
            this.txtCodigo.Enabled = false;
            this.txtCodigo.Location = new System.Drawing.Point(132, 16);
            this.txtCodigo.MaxLength = 2;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Size = new System.Drawing.Size(131, 20);
            this.txtCodigo.TabIndex = 0;
            // 
            // TxtCep
            // 
            this.TxtCep.Location = new System.Drawing.Point(438, 184);
            this.TxtCep.Mask = "00.000-000";
            this.TxtCep.Name = "TxtCep";
            this.TxtCep.Size = new System.Drawing.Size(108, 20);
            this.TxtCep.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(394, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 15);
            this.label12.TabIndex = 79;
            this.label12.Text = "Cep :";
            // 
            // TxtTelefoneCelular
            // 
            this.TxtTelefoneCelular.Location = new System.Drawing.Point(397, 128);
            this.TxtTelefoneCelular.Mask = "(00)0000-0000";
            this.TxtTelefoneCelular.Name = "TxtTelefoneCelular";
            this.TxtTelefoneCelular.Size = new System.Drawing.Size(149, 20);
            this.TxtTelefoneCelular.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(338, 130);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 15);
            this.label11.TabIndex = 78;
            this.label11.Text = "Celular :";
            // 
            // TxtTelefone
            // 
            this.TxtTelefone.Location = new System.Drawing.Point(132, 128);
            this.TxtTelefone.Mask = "(00)0000-0000";
            this.TxtTelefone.Name = "TxtTelefone";
            this.TxtTelefone.Size = new System.Drawing.Size(131, 20);
            this.TxtTelefone.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(65, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 15);
            this.label10.TabIndex = 77;
            this.label10.Text = "Telefone :";
            // 
            // TxtCPFCNPJ
            // 
            this.TxtCPFCNPJ.Location = new System.Drawing.Point(132, 100);
            this.TxtCPFCNPJ.Mask = "000.000.000-00";
            this.TxtCPFCNPJ.Name = "TxtCPFCNPJ";
            this.TxtCPFCNPJ.Size = new System.Drawing.Size(131, 20);
            this.TxtCPFCNPJ.TabIndex = 3;
            // 
            // lblRG_InscricaoEstadual
            // 
            this.lblRG_InscricaoEstadual.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRG_InscricaoEstadual.Location = new System.Drawing.Point(269, 102);
            this.lblRG_InscricaoEstadual.Name = "lblRG_InscricaoEstadual";
            this.lblRG_InscricaoEstadual.Size = new System.Drawing.Size(122, 15);
            this.lblRG_InscricaoEstadual.TabIndex = 76;
            this.lblRG_InscricaoEstadual.Text = "RG :";
            this.lblRG_InscricaoEstadual.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRG_InscricaoEstadual
            // 
            this.TxtRG_InscricaoEstadual.BackColor = System.Drawing.Color.White;
            this.TxtRG_InscricaoEstadual.Enabled = false;
            this.TxtRG_InscricaoEstadual.Location = new System.Drawing.Point(397, 100);
            this.TxtRG_InscricaoEstadual.Name = "TxtRG_InscricaoEstadual";
            this.TxtRG_InscricaoEstadual.Size = new System.Drawing.Size(149, 20);
            this.TxtRG_InscricaoEstadual.TabIndex = 4;
            // 
            // lblCPF_CNPJ
            // 
            this.lblCPF_CNPJ.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF_CNPJ.Location = new System.Drawing.Point(62, 102);
            this.lblCPF_CNPJ.Name = "lblCPF_CNPJ";
            this.lblCPF_CNPJ.Size = new System.Drawing.Size(64, 15);
            this.lblCPF_CNPJ.TabIndex = 75;
            this.lblCPF_CNPJ.Text = "CPF :";
            this.lblCPF_CNPJ.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(435, 242);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 74;
            this.label7.Text = "Estado :";
            // 
            // TxtEstado
            // 
            this.TxtEstado.BackColor = System.Drawing.Color.White;
            this.TxtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxtEstado.Enabled = false;
            this.TxtEstado.Location = new System.Drawing.Point(493, 240);
            this.TxtEstado.MaxLength = 2;
            this.TxtEstado.Name = "TxtEstado";
            this.TxtEstado.Size = new System.Drawing.Size(53, 20);
            this.TxtEstado.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(74, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 15);
            this.label6.TabIndex = 73;
            this.label6.Text = "Cidade :";
            // 
            // TxtCidade
            // 
            this.TxtCidade.BackColor = System.Drawing.Color.White;
            this.TxtCidade.Enabled = false;
            this.TxtCidade.Location = new System.Drawing.Point(132, 240);
            this.TxtCidade.Name = "TxtCidade";
            this.TxtCidade.Size = new System.Drawing.Size(297, 20);
            this.TxtCidade.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(78, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 72;
            this.label5.Text = "Bairro :";
            // 
            // TxtBairro
            // 
            this.TxtBairro.BackColor = System.Drawing.Color.White;
            this.TxtBairro.Enabled = false;
            this.TxtBairro.Location = new System.Drawing.Point(132, 212);
            this.TxtBairro.Name = "TxtBairro";
            this.TxtBairro.Size = new System.Drawing.Size(414, 20);
            this.TxtBairro.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(68, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 15);
            this.label4.TabIndex = 71;
            this.label4.Text = "Número :";
            // 
            // TxtEnderecoNumero
            // 
            this.TxtEnderecoNumero.BackColor = System.Drawing.Color.White;
            this.TxtEnderecoNumero.Enabled = false;
            this.TxtEnderecoNumero.Location = new System.Drawing.Point(132, 184);
            this.TxtEnderecoNumero.Name = "TxtEnderecoNumero";
            this.TxtEnderecoNumero.Size = new System.Drawing.Size(255, 20);
            this.TxtEnderecoNumero.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(59, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 70;
            this.label3.Text = "Endereço :";
            // 
            // TxtEndereço
            // 
            this.TxtEndereço.BackColor = System.Drawing.Color.White;
            this.TxtEndereço.Enabled = false;
            this.TxtEndereço.Location = new System.Drawing.Point(132, 156);
            this.TxtEndereço.Name = "TxtEndereço";
            this.TxtEndereço.Size = new System.Drawing.Size(414, 20);
            this.TxtEndereço.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(82, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 15);
            this.label2.TabIndex = 69;
            this.label2.Text = "Email :";
            // 
            // TxtEmail
            // 
            this.TxtEmail.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Enabled = false;
            this.TxtEmail.Location = new System.Drawing.Point(132, 72);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(414, 20);
            this.TxtEmail.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(80, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 68;
            this.label1.Text = "Nome :";
            // 
            // TxtNome
            // 
            this.TxtNome.BackColor = System.Drawing.Color.White;
            this.TxtNome.Enabled = false;
            this.TxtNome.Location = new System.Drawing.Point(132, 44);
            this.TxtNome.Name = "TxtNome";
            this.TxtNome.Size = new System.Drawing.Size(414, 20);
            this.TxtNome.TabIndex = 1;
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblStatus});
            this.StatusStrip.Location = new System.Drawing.Point(0, 659);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(822, 22);
            this.StatusStrip.TabIndex = 83;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // LblStatus
            // 
            this.LblStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(124, 17);
            this.LblStatus.Text = "Status : Consultando";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(44, 302);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 15);
            this.label14.TabIndex = 84;
            this.label14.Text = "Observação :";
            // 
            // txtObservacao
            // 
            this.txtObservacao.BackColor = System.Drawing.Color.White;
            this.txtObservacao.Enabled = false;
            this.txtObservacao.Location = new System.Drawing.Point(132, 302);
            this.txtObservacao.MaxLength = 255;
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtObservacao.Size = new System.Drawing.Size(414, 56);
            this.txtObservacao.TabIndex = 15;
            this.txtObservacao.TextChanged += new System.EventHandler(this.txtObservacao_TextChanged);
            // 
            // cmbClinica
            // 
            this.cmbClinica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClinica.FormattingEnabled = true;
            this.cmbClinica.Location = new System.Drawing.Point(324, 15);
            this.cmbClinica.Name = "cmbClinica";
            this.cmbClinica.Size = new System.Drawing.Size(222, 21);
            this.cmbClinica.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(267, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 15);
            this.label9.TabIndex = 122;
            this.label9.Text = "Clinica :";
            // 
            // cmbProfissional
            // 
            this.cmbProfissional.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProfissional.FormattingEnabled = true;
            this.cmbProfissional.Location = new System.Drawing.Point(132, 267);
            this.cmbProfissional.Name = "cmbProfissional";
            this.cmbProfissional.Size = new System.Drawing.Size(161, 21);
            this.cmbProfissional.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(45, 269);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 15);
            this.label8.TabIndex = 124;
            this.label8.Text = "Profissional :";
            // 
            // cmbConvenio
            // 
            this.cmbConvenio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConvenio.FormattingEnabled = true;
            this.cmbConvenio.Location = new System.Drawing.Point(370, 267);
            this.cmbConvenio.Name = "cmbConvenio";
            this.cmbConvenio.Size = new System.Drawing.Size(176, 21);
            this.cmbConvenio.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(299, 269);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 15);
            this.label15.TabIndex = 126;
            this.label15.Text = "Convênio :";
            // 
            // lblCaracteresRestantes
            // 
            this.lblCaracteresRestantes.AutoSize = true;
            this.lblCaracteresRestantes.Location = new System.Drawing.Point(419, 360);
            this.lblCaracteresRestantes.Name = "lblCaracteresRestantes";
            this.lblCaracteresRestantes.Size = new System.Drawing.Size(127, 13);
            this.lblCaracteresRestantes.TabIndex = 136;
            this.lblCaracteresRestantes.Text = "255 caracteres restantes.";
            // 
            // ctrNavigator1
            // 
            this.ctrNavigator1.DataSource = null;
            this.ctrNavigator1.Indice = -1;
            this.ctrNavigator1.Location = new System.Drawing.Point(25, 373);
            this.ctrNavigator1.Name = "ctrNavigator1";
            this.ctrNavigator1.Size = new System.Drawing.Size(547, 42);
            this.ctrNavigator1.TabIndex = 16;
            this.ctrNavigator1.MudaRegistroSelecionado += new ControlePacientes.Controles.CtrNavigator.MudaRegistro(this.ctrNavigator1_MudaRegistroSelecionado);
            this.ctrNavigator1.EventoNovo += new ControlePacientes.Controles.CtrNavigator.Novo(this.ctrNavigator1_EventoNovo);
            this.ctrNavigator1.CancelarAcao += new ControlePacientes.Controles.CtrNavigator.Cancelar(this.ctrNavigator1_CancelarAcao);
            this.ctrNavigator1.EditarRegistro += new ControlePacientes.Controles.CtrNavigator.Editar(this.ctrNavigator1_EditarRegistro);
            this.ctrNavigator1.SalvarRegistro += new ControlePacientes.Controles.CtrNavigator.Salvar(this.ctrNavigator1_SalvarRegistro);
            this.ctrNavigator1.ExcluirRegistro += new ControlePacientes.Controles.CtrNavigator.Excluir(this.ctrNavigator1_ExcluirRegistro);
            // 
            // CadastroPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 681);
            this.Controls.Add(this.lblCaracteresRestantes);
            this.Controls.Add(this.cmbConvenio);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cmbProfissional);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmbClinica);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtObservacao);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.DGPacientes);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.TxtCep);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TxtTelefoneCelular);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TxtTelefone);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TxtCPFCNPJ);
            this.Controls.Add(this.lblRG_InscricaoEstadual);
            this.Controls.Add(this.TxtRG_InscricaoEstadual);
            this.Controls.Add(this.lblCPF_CNPJ);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtEstado);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtCidade);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtBairro);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtEnderecoNumero);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtEndereço);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.ctrNavigator1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtNome);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CadastroPaciente";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Pacientes";
            this.Load += new System.EventHandler(this.CadastroPaciente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGPacientes)).EndInit();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DGPacientes;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.MaskedTextBox TxtCep;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox TxtTelefoneCelular;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox TxtTelefone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox TxtCPFCNPJ;
        private System.Windows.Forms.Label lblRG_InscricaoEstadual;
        private System.Windows.Forms.TextBox TxtRG_InscricaoEstadual;
        private System.Windows.Forms.Label lblCPF_CNPJ;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtEstado;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtCidade;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtBairro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtEnderecoNumero;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtEndereço;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtEmail;
        private Controles.CtrNavigator ctrNavigator1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtNome;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel LblStatus;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.ComboBox cmbClinica;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbProfissional;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbConvenio;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblCaracteresRestantes;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNome;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTelefoneClinica;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColTelefoneFax;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCNPJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColInscricaoEstadual;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNomeFantasia;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColEndereco;
    }
}

