﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private NHibernate.ISession _Session;
        private BubbleSort.Servicos.TratadorDeErro _TratadorErro;
        private BubbleSort.Repositorio.ConsultaRepositorio _ConsultaRepositorio;

        private void clinicasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CadastroClinicas(_TratadorErro, _Session).ShowDialog();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            if (new TelaLogin().ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    _TratadorErro = new BubbleSort.Servicos.TratadorDeErro();
                    _Session = BubbleSort.Repositorio.SessionHelper.NovaSessao;
                    _ConsultaRepositorio = new BubbleSort.Repositorio.ConsultaRepositorio(_Session);
                    //Sincronizar();
                    System.Threading.Thread thread = new System.Threading.Thread(CarregarConsultas);
                    thread.Start();
                }
                catch (Exception ex) { if (_TratadorErro != null) _TratadorErro.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica); }
            }
            else
                this.Close();
        }

        private void pacientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CadastroPaciente(_TratadorErro, _Session).ShowDialog();
        }

        private void profissionaisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CadastroProfissionais(_TratadorErro, _Session).ShowDialog();
        }

        private void convêniosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CadastroConvenios(_TratadorErro, _Session).ShowDialog();
        }

        private void examesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CadastroExames(_TratadorErro, _Session).ShowDialog();
        }

        private void consultasDoPacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ConsultasPaciente(_TratadorErro, _Session).ShowDialog();
        }

        private void relatórioDePacientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Relatorio.RelatorioPacientes(_TratadorErro, _Session).ShowDialog();
        }

        private void relatórioDeConsultasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Relatorio.RelatorioConsultas(_TratadorErro, _Session).ShowDialog();
        }

        private void CarregarConsultas()
        {
            this.BeginInvoke((Action)(() =>
            {
                LblStatus.Text = "Carregando consultas";
            }));
            var inicio = DateTime.Now.Date;
            var fim = DateTime.Now.Date.AddHours(24);
            var proximasConsultas = _ConsultaRepositorio.BuscarPor(x => x.Data > inicio && x.Data < fim, x => x.Data);
            this.BeginInvoke((Action)(() =>
            {
                gdvConsultasHoje.AutoGenerateColumns = false;
                gdvConsultasHoje.DataSource = proximasConsultas;
            }));

            this.BeginInvoke((Action)(() =>
            {
                LblStatus.Text = "Finalizado";
            }));
        }

        private void Sincronizar()
        {
            Servicos.Sincronizacao sincronizar = new Servicos.Sincronizacao();
            sincronizar.SynchronizeUp();
        }

        private void gdvConsultasHoje_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 3)//Consulta{
                {
                    var consulta = (BubbleSort.Dominio.Consultas)gdvConsultasHoje.Rows[e.RowIndex].DataBoundItem;
                    new NovaConsulta(_TratadorErro, _Session,consulta.Paciente, consulta).ShowDialog();
                }
                else if (e.ColumnIndex == 4)//Paciente
                {
                    new CadastroPaciente(_TratadorErro, _Session).ShowDialog();
                }
            }
        }

        private void toolStripProfissionais_Click(object sender, EventArgs e)
        {
            new CadastroProfissionais(_TratadorErro, _Session).ShowDialog();
        }

        private void toolStripCadastroPacientes_Click(object sender, EventArgs e)
        {
            new CadastroPaciente(_TratadorErro, _Session).ShowDialog();
        }

        private void toolStripCadastroConsulta_Click(object sender, EventArgs e)
        {
            new ConsultasPaciente(_TratadorErro, _Session).ShowDialog();
        }

        private void relatórioDeConsultaDetalhadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Relatorio.RelatorioConsultasDetalhado(_TratadorErro, _Session).ShowDialog();
        }

        private void btnAtualizarConsultas_Click(object sender, EventArgs e)
        {
            System.Threading.Thread thread = new System.Threading.Thread(CarregarConsultas);
            thread.Start();
        }

        private void logotipoRelatóriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Logotipo().ShowDialog();
        }

        private void sincronizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Sincronizar().ShowDialog();
        }

    }
}
