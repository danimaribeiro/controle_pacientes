﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlePacientes
{
    public partial class Sincronizar : Form
    {
        public Sincronizar()
        {
            InitializeComponent();
        }

        private void Sincronizar_Load(object sender, EventArgs e)
        {
            try
            {
                Servicos.Sincronizacao sincronia = new Servicos.Sincronizacao();
                sincronia.SyncProgress += new EventHandler<Microsoft.Synchronization.Data.SyncProgressEventArgs>(sincronia_SyncProgress);
                sincronia.SynchronizeUp();
            }
            catch (Exception ex)
            {
                BubbleSort.Servicos.TratadorDeErro tratador = new BubbleSort.Servicos.TratadorDeErro();
                tratador.TrataOErro(ex, BubbleSort.Servicos.TratadorDeErro.TipoErro.InterfaceGrafica);
            }
        }

        void sincronia_SyncProgress(object sender, Microsoft.Synchronization.Data.SyncProgressEventArgs e)
        {
            label1.Text = "Modificações: " + e.GroupProgress.TotalChanges;
        }
    }
}
