﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio.Sincronizacao
{
    public class RegistrosAtualizar:IEntidade
    {
        public virtual string Id { get; set; }

        public virtual long Codigo { get; set; }

        public virtual DateTime DataAtualizacao { get; set; }

        public virtual string Tabela { get; set; }

        public virtual string uuid { get; set; }

        public virtual Enumeradores.TipoUpdate TipoAtualizacao { get; set; } 
    }
}
