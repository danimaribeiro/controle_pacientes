﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio.Sincronizacao
{
    public class UltimaAtualizacao:IEntidade
    {
        public virtual string Id { get; set; }

        public virtual long Codigo { get; set; }

        public virtual DateTime DataAtualizacao { get; set; }
    }
}
