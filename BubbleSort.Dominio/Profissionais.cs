﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class Profissionais:IEntidade
    {
        public Profissionais() { Clinica = new Clinicas(); Nome = string.Empty; Endereco = new Endereco(); Cpf = string.Empty; Rg = string.Empty; Email = string.Empty; DataNascimento = DateTime.Now.AddYears(-25); TelefoneCelular = string.Empty; TelefoneFixo = string.Empty; Observacao = string.Empty; }

        public override string ToString()
        {
            return Nome;
        }

        public virtual string Id { get; set; }

        public virtual long Codigo { get; set; }

        public virtual Clinicas Clinica { get; set; }

        public virtual string Nome { get; set; }

        public virtual Endereco Endereco { get; set; }

        public virtual string Cpf { get; set; }

        public virtual string Rg { get; set; }

        public virtual string Email { get; set; }

        public virtual DateTime DataNascimento { get; set; }

        public virtual string TelefoneFixo { get; set; }

        public virtual string TelefoneCelular { get; set; }

        public virtual string Observacao { get; set; }
         
    }
}
