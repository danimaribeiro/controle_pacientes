﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using BubbleSort.Dominio.UoW;

namespace BubbleSort.Dominio.Repositorio
{
    public interface IRepositorio<TEntidade> : IDisposable where TEntidade : IEntidade
    {
        IUnitOfWork UnitOfWork { get; set; }

        IEnumerable<TEntidade> Todos();

        IList<TEntidade> Todos(System.Linq.Expressions.Expression<Func<TEntidade, object>> path);

        IEnumerable<TEntidade> Todos(int pagina, int quantidade);

        string Adicionar(TEntidade entidade);

        string AdicionarSemLog(TEntidade entidade);

        void Deletar(TEntidade entidade);

        void Atualizar(TEntidade entidade);

        void AtualizarSemLog(TEntidade entidade);

        TEntidade BuscarPorId(string id);

        TEntidade CarregarPorId(string id);

        IEnumerable<TEntidade> BuscarPor(Expression<Func<TEntidade, bool>> condicao);

        IEnumerable<TEntidade> BuscarPor(System.Linq.Expressions.Expression<Func<TEntidade, bool>> condicao, System.Linq.Expressions.Expression<Func<TEntidade, object>> path);

        TEntidade BuscarRegistro(System.Linq.Expressions.Expression<Func<TEntidade, bool>> condicao);

        long ProximoCodigo();
    }
}
