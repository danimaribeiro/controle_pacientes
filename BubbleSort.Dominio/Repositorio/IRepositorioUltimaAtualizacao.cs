﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio.Repositorio
{
    public interface IRepositorioUltimaAtualizacao : IRepositorio<BubbleSort.Dominio.Sincronizacao.UltimaAtualizacao>
    {
    }
}
