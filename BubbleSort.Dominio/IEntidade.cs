﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public interface IEntidade
    {
        string Id { get; set; }

        long Codigo { get; set; }
    }
}
