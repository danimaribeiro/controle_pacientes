﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class Clinicas : IEntidade
    {       
        public Clinicas() 
        {  
            Nome = String.Empty; 
            NomeFantasia = String.Empty; 
            Endereco = new Endereco(); 
            Cnpj = String.Empty; 
            InscricaoEstadual = string.Empty; 
            TelefoneClinica = string.Empty; 
            TelefoneFax = string.Empty;                     
        }

        public override string ToString()
        {
            return this.Nome;
        }

        public virtual string Id { get; set; }

        public virtual long Codigo { get; set; }
                
        public virtual string Nome { get; set; }

        public virtual string NomeFantasia { get; set; }

        public virtual Endereco Endereco { get; set; }

        public virtual string Cnpj { get; set; }

        public virtual string InscricaoEstadual { get; set; }

        public virtual string TelefoneClinica { get; set; }

        public virtual string TelefoneFax { get; set; }

    }
}
