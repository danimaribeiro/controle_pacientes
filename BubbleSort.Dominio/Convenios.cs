﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class Convenios:IEntidade
    {
        public Convenios()
        {                     
            this.clinica = new Clinicas();
            this.nomeConvenio = string.Empty;
            this.observacao = string.Empty;
        }

        public override string ToString()
        {
            return this.nomeConvenio;
        }

        public virtual string Id { get; set; }
        public virtual long Codigo { get; set; }
        public virtual Clinicas clinica { get; set; }
        public virtual string nomeConvenio { get; set; }
        public virtual string observacao { get; set; }
    }
}
