﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class Paciente : IEntidade
    {
        public Paciente() { Nome = string.Empty; Endereco = new Endereco(); Cpf = string.Empty; Rg = string.Empty; TelefoneFixo = string.Empty; TelefoneCelular = string.Empty; Email = string.Empty; Observacao = string.Empty; }

        public override string ToString()
        {
            return Nome;
        }

        public virtual string Id { get; set; }

        public virtual long Codigo { get; set; }
        
        public virtual string Nome { get; set; }

        public virtual Endereco Endereco { get; set; }

        public virtual string Cpf { get; set; }

        public virtual string Rg { get; set; }

        public virtual string TelefoneFixo { get; set; }

        public virtual string TelefoneCelular { get; set; }

        public virtual string Email { get; set; }

        public virtual string Observacao { get; set; }

        public virtual string NomeClinica { get { return this.Clinica == null ? string.Empty : this.Clinica.Nome; } }

        public virtual Clinicas Clinica { get; set; }

        public virtual string NomeConvenio { get { return this.Convenio == null ? string.Empty : this.Convenio.nomeConvenio; } }

        public virtual Convenios Convenio { get; set; }

        public virtual Profissionais Profissional { get; set; }
    }
}
