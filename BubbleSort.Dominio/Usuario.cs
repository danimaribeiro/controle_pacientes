﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class Usuario: IEntidade
    {
        public virtual string Id{ get; set; }

        public virtual long Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Username { get; set; }

        public virtual string Senha { get; set; }
    }
}
