﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        IUnitOfWork BeginTransaction(System.Data.IsolationLevel isolation);

        void Commit();

        void Rollback();
    }
}
