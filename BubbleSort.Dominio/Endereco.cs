﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class Endereco : IEntidade
    {
        public Endereco()
        {
            Rua = string.Empty;
            Numero = string.Empty; 
            Bairro = string.Empty; 
            Cidade = string.Empty; 
            Estado = string.Empty; 
            Cep = string.Empty;
        }

        public virtual string Id { get; set; }

        public virtual long Codigo { get; set; }

        public virtual string Rua { get; set; }

        public virtual string Numero { get; set; }

        public virtual string Bairro { get; set; }

        public virtual string Cidade { get; set; }

        public virtual string Estado { get; set; }

        public virtual string Cep { get; set; }

    }
}
