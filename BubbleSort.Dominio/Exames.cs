﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class Exames:IEntidade
    {
        public override string ToString()
        {
            return nomeExame;
        }
        public virtual string Id { get; set; }
        public virtual long Codigo { get; set; }
        public virtual Clinicas clinica { get; set; }
        public virtual string nomeExame { get; set; }
        public virtual string observacao { get; set; }
    }
}
