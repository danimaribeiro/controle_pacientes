﻿namespace BubbleSort.Dominio.Enumeradores
{
    public enum TipoConsulta
    {
        Consulta = 0,
        Retorno = 1
    }

    public enum TipoUpdate
    {
        Insert=0,
        Update=1,
        Delete=2
    }
}