﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class ExameConsulta: IEntidade
    {
        public virtual string Id { get; set; }

        public virtual long Codigo { get; set; }

        public virtual Consultas Consulta { get; set; }

        public virtual Exames Exame { get; set; }

        public virtual string Observacao { get; set; }

        public virtual string NomeClinica { get { return this.Exame == null ? string.Empty : this.Exame.clinica.Nome; } }

        public virtual string NomeExame { get { return this.Exame == null ? string.Empty : this.Exame.nomeExame; } }
    }
}
