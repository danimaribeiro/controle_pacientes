﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BubbleSort.Dominio
{
    public class Consultas : IEntidade
    {

        public override string ToString()
        {
            return this.Data.ToString("dd/MM/yyyy hh:mm") + " - " + this.NomePaciente;
        }

        public virtual string Id { get; set; }

        public virtual long Codigo { get; set; }

        public virtual string NomeClinica { get { return this.Clinica == null ? string.Empty : this.Clinica.Nome; } }

        public virtual Clinicas Clinica { get; set; }

        public virtual string NomePaciente { get { return this.Paciente == null ? string.Empty : this.Paciente.Nome; } }

        public virtual Paciente Paciente { get; set; }

        public virtual DateTime Data { get; set; }

        public virtual Enumeradores.TipoConsulta Tipo { get; set;}

        public virtual string NomeProfissional { get{ return this.Profissional == null ? string.Empty : this.Profissional.Nome; } }

        public virtual Profissionais Profissional { get; set; }

        public virtual string Observacoes { get; set; }

        public virtual IList<Dominio.ExameConsulta> ListaExames { get; set; }

        public virtual int status { get; set; }// 0:Aberta, 1: Finalizada
    }
}
