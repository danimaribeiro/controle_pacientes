﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BubbleSort.Servicos
{
    public class GravaMensagemLog
    {
        public void GravaLogEmTxt(Exception ex)
        {
            try
            {
                string pasta = System.IO.Path.Combine(Application.StartupPath, "Log");

                // se não existe a pasta cria a ela no startap da aplicação
                if (!System.IO.Directory.Exists(pasta))
                    System.IO.Directory.CreateDirectory(pasta);

                // recebe a data atual para gravar o log
                System.DateTime dt = DateTime.Now;
                string file = dt.Day + "-" + dt.Month + "-" + dt.Year + ".txt";

                pasta = System.IO.Path.Combine(pasta, file);

                System.IO.StreamWriter strw = new System.IO.StreamWriter(pasta, true);
                try
                {
                    strw.WriteLine(DateTime.Now.ToShortTimeString());
                    strw.WriteLine(ex.ToString());
                    strw.WriteLine();
                }
                catch
                {
                }
                finally
                {
                    strw.Close();
                }
            }
            catch
            {
            }
        }
    }
}
