﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BubbleSort.Servicos
{
    public class TratadorDeErro
    {
        private GravaMensagemLog _Log;
        public enum TipoErro { InterfaceGrafica = 0, Repositorio = 1, Dominio = 2, Servicos = 3 }

        public TratadorDeErro()
        {
            _Log = new GravaMensagemLog();
        }

        public void TrataOErro(Exception ex, TipoErro tipo)
        {
            try
            {
                switch (tipo)
                {
                    case TipoErro.InterfaceGrafica:
                        TrataErroInterface(ex);
                        break;
                    case TipoErro.Dominio:
                        TrataErroDominio(ex);
                        break;
                    case TipoErro.Repositorio:
                        TrataErroRepositorio(ex);
                        break;
                    case TipoErro.Servicos:
                        TrataErroServicos(ex);
                        break;
                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.Message);
            }
        }

        private void TrataErroInterface(Exception ex)
        {
            if (object.ReferenceEquals(ex.GetType(), typeof(System.Net.WebException)))
            {
                MessageBox.Show("Não foi possível efetuar a requisição por falha de internet. Por favor tente novamente!", "Falha de Internet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (object.ReferenceEquals(ex.GetType(), typeof(System.Net.Sockets.SocketException)))
            {
                MessageBox.Show("Não foi possível efetuar a requisição por falha de internet. Por favor tente novamente!", "Falha de Internet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (object.ReferenceEquals(ex.GetType(), typeof(System.Net.HttpListenerException)))
            {
                MessageBox.Show("Não foi possível efetuar a requisição por falha de internet. Por favor tente novamente!", "Falha de Internet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //else if (object.ReferenceEquals(ex.GetType(), typeof(System.Web.Services.Protocols.SoapException)))
            //{
            //    MostraMensagemDoRepositorio(ex.Message);
            //}
            else
            {
                MostraMensagemDoRepositorio(ex.Message);
            }
            _Log.GravaLogEmTxt(ex);
        }

        private void TrataErroDominio(Exception ex)
        {
            _Log.GravaLogEmTxt(ex);
        }

        private void TrataErroRepositorio(Exception ex)
        {
            _Log.GravaLogEmTxt(ex);
        }

        private void TrataErroServicos(Exception ex)
        {
            _Log.GravaLogEmTxt(ex);
        }

        private void MostraMensagemDoRepositorio(string mensagem)
        {
            int first = mensagem.IndexOf("#");
            int last = mensagem.IndexOf("#", first + 1);
            if (first != -1 & last != -1)
            {
                mensagem = mensagem.Substring(first + 1, last - (first + 1));
            }

            MessageBox.Show(mensagem, "Atenção!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }
}
